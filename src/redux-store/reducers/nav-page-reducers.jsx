import {SET_NAV_PAGE} from './../actions/nav-page-actions';

const getDefaultState = () => ({});

export const navPage = (state = getDefaultState(), action) => {
    const {type, payload} = action;
    console.log('current state');
    console.log(state);
    switch (type) {
        case SET_NAV_PAGE: {
            const navPage = payload.navPage;
            const navPageSource = payload.navPageSource;
            const tableItem = payload.tableItem;
            return Object.assign({}, state, { navPage,navPageSource, tableItem});
        }

        default: {
            return state;
        }
    }

};

