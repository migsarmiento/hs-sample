import {SHOW_CONNECT_MODAL, HIDE_CONNECT_MODAL} from './../actions/modal-actions';
import {
    HIDE_NEW_STAK_MODAL, HIDE_SAVE_SEARCH_MODAL, HIDE_PREVIEW_STAK_MODAL,
    SHOW_NEW_STAK_MODAL, SHOW_SAVE_SEARCH_MODAL, SHOW_PREVIEW_STAK_MODAL
} from "../actions/modal-actions";

const getDefaultState = () => ({});

export const modal = (state = getDefaultState(), action) => {
    const {type, payload} = action;
    switch (type) {

        case SHOW_CONNECT_MODAL: {
            const showConnectModal = true;
            return Object.assign({}, state, {showConnectModal});
        }

        case HIDE_CONNECT_MODAL: {
            const showConnectModal = false;
            return Object.assign({}, state, {showConnectModal});
        }
        case SHOW_NEW_STAK_MODAL: {
            const showNewStakModal = true;
            const tableItem = payload.tableItem;
            return Object.assign({}, state, {showNewStakModal,tableItem});
        }

        case HIDE_NEW_STAK_MODAL: {
            const showNewStakModal = false;
            return Object.assign({}, state, {showNewStakModal});
        }

        case SHOW_SAVE_SEARCH_MODAL: {
            const showSaveSearchModal = true;
            return Object.assign({}, state, {showSaveSearchModal});
        }

        case HIDE_SAVE_SEARCH_MODAL: {
            const showSaveSearchModal = false;
            return Object.assign({}, state, {showSaveSearchModal,});
        }

        case SHOW_PREVIEW_STAK_MODAL: {
            const showPreviewStakModal = true;
            const tableItem = payload.tableItem;
            return Object.assign({}, state, {showPreviewStakModal, tableItem});
        }

        case HIDE_PREVIEW_STAK_MODAL: {
            const showPreviewStakModal = false;
            return Object.assign({}, state, {showPreviewStakModal});
        }

        default: {
            return state;
        }
    }
}