import {SET_SEARCH_QUERY, SET_SEARCH_RESULTS, SET_SAVED_SEARCH_ITEMS, SET_RECENT_SEARCHED_ITEMS} from './../actions/search-actions';

const getDefaultState = () => ({});

export const search = (state = getDefaultState(), action) => {
    const {type, payload} = action;
    console.log('current state');
    console.log(state);
    switch (type) {

        case SET_RECENT_SEARCHED_ITEMS: {
            const recentSearchedItems = payload.recentSearchedItems;
            return Object.assign({}, state, { recentSearchedItems });
        }

        case SET_SEARCH_QUERY: {
            const searchQuery = payload.searchQuery;
            return Object.assign({}, state, { searchQuery });
        }

        case SET_SEARCH_RESULTS: {
            const searchResults = payload.searchResults;
            return Object.assign({}, state, { searchResults });
        }

        case SET_SAVED_SEARCH_ITEMS: {
            const savedSearchItems = payload.savedSearchItems;
            return Object.assign({}, state, {savedSearchItems});
        }

        default: {
            return state;
        }
    }

};

