import { combineReducers } from 'redux';
import { search } from './reducers/search-reducers';
import { navPage } from './reducers/nav-page-reducers';
import { modal } from './reducers/modal-reducer';

const reducers = combineReducers({
    search,
    navPage,
    modal
});

export default reducers;