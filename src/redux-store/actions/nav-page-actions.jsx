export const SET_NAV_PAGE = 'SET_NAV_PAGE';

export const setNavPage = (navPage, navPageSource, tableItem) => {
    console.log('SET NAV PAGE');
    return {
        type: SET_NAV_PAGE,
        payload: {navPage,navPageSource, tableItem}
    }
}