export const SET_SEARCH_QUERY = 'SET_SEARCH_QUERY';

export const SET_SEARCH_RESULTS = 'SET_SEARCH_RESULTS';

export const SET_SAVED_SEARCH_ITEMS = 'SET_SAVED_SEARCH_ITEMS';

export const SET_RECENT_SEARCHED_ITEMS = 'SET_RECENT_SEARCHED_ITEMS';

export const setRecentSearchedItems = (recentSearchedItems) => {
    console.log('toggle recent saerch items');
    return {
        type: SET_RECENT_SEARCHED_ITEMS,
        payload: {recentSearchedItems}
    }
}

export const setSearchQuery = (searchQuery) => {
    console.log('TOGGLE SEARCH QUERY');
    return {
        type: SET_SEARCH_QUERY,
        payload: {searchQuery}
    };
}

export const setSearchResults = (searchResults) => {
    console.log('LOAD SEARCH RESULTS');
    return {
        type: SET_SEARCH_RESULTS,
        payload: {searchResults}
    }
}

export const setSavedSearchItems = (savedSearchItems) => {
    console.log('LOAD SAVED SEARCH ITEMS');
    return {
        type: SET_SAVED_SEARCH_ITEMS,
        payload: {savedSearchItems}
    }
}