export const SHOW_NEW_STAK_MODAL = 'SHOW_NEW_STAK_MODAL';
export const HIDE_NEW_STAK_MODAL = 'HIDE_NEW_STACK_MODAL';
export const SHOW_EDIT_STAK_MODAL = 'SHOW_NEW_STAK_MODAL';
export const HIDE_EDIT_STAK_MODAL = 'HIDE_NEW_STACK_MODAL';
export const SHOW_CONNECT_MODAL = 'SHOW_CONNECT_MODAL';
export const HIDE_CONNECT_MODAL = 'HIDE_CONNECT_MODAL';
export const SHOW_SAVE_SEARCH_MODAL = 'SHOW_SAVE_SEARCH_MODAL';
export const HIDE_SAVE_SEARCH_MODAL = 'HIDE_SAVE_SEARCH_MODAL';
export const SHOW_PREVIEW_STAK_MODAL = 'SHOW_PREVIEW_STAK_MODAL';
export const HIDE_PREVIEW_STAK_MODAL = 'HIDE_PREVIEW_STAK_MODAL'


export const showNewStakModal = (tableItem) => {
    console.log('show new stak modal');
    return {
        type: SHOW_NEW_STAK_MODAL,
        payload: { tableItem }
    };
}

export const hideNewStakModal = () => {
    return {
        type: HIDE_NEW_STAK_MODAL
    }
}

export const showEditStakModal = (tableItem) => {
    return {
        type: SHOW_EDIT_STAK_MODAL,
        payload: { tableItem }
    };
}

export const hideEditStakModal = () => {
    return {
        type: HIDE_EDIT_STAK_MODAL
    }
}


export const showConnectModal = () => {
    console.log('SHOW CONNECT');
    return {
        type: SHOW_CONNECT_MODAL
    }
}

export const hideConnectModal = () => {
    return {
        type: HIDE_CONNECT_MODAL
    }
}


export const showSaveSearchModal = () => {
    return {
        type: SHOW_SAVE_SEARCH_MODAL
    }
}

export const hideSaveSearchModal = () => {
    return {
        type: HIDE_SAVE_SEARCH_MODAL
    }
}


export const showPreviewStakModal = (tableItem) => {
    return {
        type: SHOW_PREVIEW_STAK_MODAL,
        payload: { tableItem } 
    }
}

export const hidePreviewStakModal = () => {
    return {
        type: HIDE_PREVIEW_STAK_MODAL
    }
}