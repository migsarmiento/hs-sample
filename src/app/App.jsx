import React, {Component} from 'react';
import './../styles/App.css';
import './../styles/ProfilePage.css';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SearchHeader from './../components/SearchHeader';
import SideNavigation from './../components/SideNavigation';
import Upload from './../components/Upload';
import Filters from './../components/Filters';
import HomeFolder from "../components/HomeFolder";
import StakFolder from "../components/StakFolder";
import FileFolder from "../components/FileFolder";
import SearchResults from "../components/SearchResults";
import ProfilePage from "../components/ProfilePage";
import Login from "../components/Login";
import ConnectModal from "../components/ConnectModal";
import NewStakModal from "../components/NewStakModal";
import SaveSearchModal from "../components/SaveSearchModal";
import NewStak from "../components/NewStak";
import EditStak from "../components/EditStak";
import PreviewStakModal from "../components/PreviewStakModal";

class App extends Component {

    componentDidMount() {
        // NOTE: TEMP FIX
        document.getElementById('app').style.backgroundColor = "white";
    }

    renderView() {
        console.log(this.props.navPage);
        if (this.props.searchQuery) {
            return (<div className="content-01">
                <SearchHeader/>
                <div className="content-02">
                    <Filters/>
                    <SearchResults/>
                </div>
            </div>);
        }

        //HOME
        if (this.getParameterByName('filename')) {
            return (<div className="content-01">
                <SearchHeader/>
                <FileFolder/>
            </div>);
        }

        if (this.props.navPage === 'Stak') {
            return (<div className="content-01">
                <SearchHeader/>
                <StakFolder/>
            </div>);
        }

        if (this.props.navPage === 'Files') {
            return (<div className="content-01">
                <SearchHeader/>
                <FileFolder/>
            </div>);
        }

        if (this.props.navPage === 'Profile') {
            return (<div className="content-01">
                <SearchHeader/>
                <ProfilePage/>
            </div>);
        }

        if (this.props.navPage === 'NewStak') {
            return ( <div className="content-01">
                <NewStak pageSource={this.props.navPageSource}/>
            </div>);
        }

        if (this.props.navPage === 'EditStak') {
            return ( <div className="content-01">
                <EditStak pageSource={this.props.navPageSource} />
            </div> );
        }

        //HOME
        if (this.props.navPage === 'Home' || this.props.navPage === '') {
            return (<div className="content-01">
                <SearchHeader/>
                <FileFolder/>
            </div>);
        }

        //HOME
        if (this.props.searchQuery === '') {
            return (<div className="content-01">
                <SearchHeader/>
                <FileFolder/>
            </div>);
        }

    }

    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    render() {
        console.log(this.props);
        console.log(this.getParameterByName('filename'));
        if (((this.props.navPage === 'Login' || this.props.navPage === '') || (!this.props.searchQuery && !this.props.navPage)) && !this.getParameterByName('filename')) {
            return (
                <Login/>
            )
        } else {
            return (
                <div className='div-main-body'>
                    <SideNavigation/>
                    <ConnectModal displayModal={this.props.showConnectModal}/>
                    <NewStakModal displayModal={this.props.showNewStakModal}/>
                    <SaveSearchModal displayModal={this.props.showSaveSearchModal}/>
                    <PreviewStakModal displayModal={this.props.showPreviewStakModal}/>
                    {this.renderView()}
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    console.log('UPDATE APP STATE');
    console.log(state);
    return {
        searchQuery: state.search ? state.search.searchQuery : '',
        navPage: state.navPage ? state.navPage.navPage : '',
        navPageSource: state.navPage ? state.navPage.navPageSource : '',
        showNewStakModal: state.modal ? state.modal.showNewStakModal : false,
        showConnectModal: state.modal ? state.modal.showConnectModal : false,
        showSaveSearchModal: state.modal ? state.modal.showSaveSearchModal : false,
        showPreviewStakModal: state.modal ? state.modal.showPreviewStakModal : false
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({}, dispatch);
}

const AppContainer = connect(mapStateToProps, mapDispatchToProps)(App);

export default AppContainer;