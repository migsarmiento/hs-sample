import axios from 'axios';

const ENCAPSA_DOMAIN = 'http://api.encapsa.com/api/enCapsaGlobalAPI/';
const ENCAPSA_PROD = 'http://10.10.1.202/api/enCapsaGlobalAPI';

export function setCredentials() {
    const COMPANY_NAME = 'KDCI'
    const USER_NAME = 'Miguel'
    const EMAIL_ADDRESS = 'miguel@kdci.co'
    const USER_PASSWORD = 'pass1234'

    axios.post(ENCAPSA_PROD + '/SetCredentials/' + COMPANY_NAME + '/' + USER_NAME + '/' + EMAIL_ADDRESS + '/' + USER_PASSWORD)
        .then(function (response) {
            console.log('CREDENTIALS SUCCESSFULLY SET');
            return response.data;
        }).catch(function (response) {
            console.log('ERROR SETTING CREDENTIALS');
            return response.data;
    });
}

const USERNAME = 'username';
const PASSWORD = 'password';
export function createSecurityCredentials() {
    axios.post(ENCAPSA_PROD + '/CreateSecurityCredentials/' + USERNAME + '/' + PASSWORD)
        .then(function (response) {
            console.log('SUCCESSFULLY CREATED SECURITY CREDENTIALS');
            return response.data;
        }).catch(function (response) {
            console.log('UNABLE TO CREATE SECURITY CREDENTIALS');
            return response.data;
    });
}

export function saveSearchQuery({dateUploaded, positionNumber, recordId, searchQuery}) {
    axios.get(`${ENCAPSA_PROD}/SaveData/GLOBAL/SAVEDSEARCHESFINAL/_SAVEDSEARCH/${searchQuery}/${positionNumber}/${recordId}`);
}


export function downloadFile(fileId) {
    return axios.get(`${ENCAPSA_PROD}/GetFormData/GLOBAL/UPLOADFILE/${fileId}`);
}

export function search(searchQuery) {
    return axios.get(ENCAPSA_PROD + '/Search/GLOBAL/' + searchQuery + '/');
}

export function getFormData() {

}

export function removeFormData() {

}



export function loadTableData(table) {
    console.log('fetch table data');
    return axios.get(ENCAPSA_PROD + '/LoadTableData/GLOBAL/' + table + '/');
}

export function saveSearchData() {
    return axios.post(ENCAPSA_PROD + '/api/enCapsaGlobalAPI/Submit/FormData')
}

export function saveData(params) {
    console.log(params);
    const config = {
        headers: {
            'content-type': 'application/x-www-form-urlencoded'
        }
    }
    return axios.post('http://api.encapsa.com/api/Post/PostFile/FormData', params, config);
}