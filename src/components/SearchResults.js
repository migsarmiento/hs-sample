import React, {Component} from 'react';
import {connect} from 'react-redux';
import {setSearchQuery, setSearchResults} from './../redux-store/actions/search-actions';
import {bindActionCreators} from 'redux';
import {downloadFile} from './../services/service';
import {
    showNewStakModal,
    hideNewStakModal, showSaveSearchModal
} from "../redux-store/actions/modal-actions";
import Png from '../styles/assets/icons/png.png';
import Jpeg from '../styles/assets/icons/jpeg.png';
import WordFile from '../styles/assets/icons/word.png';
import Txt from '../styles/assets/icons/txt.png';
import RandomFile from '../styles/assets/icons/paper.png';
import AdobeFile from '../styles/assets/icons/adobe.png';
import StakGreen from '../styles/assets/Stak Green.png';
import FileDisplayLayout from '../components/FileDisplayLayout';

class SearchResults extends Component {

    constructor(props) {
        super(props);
        this.renderContextMenu = this.renderContextMenu.bind(this);
        this.setRenderContentMenuOption = this.setRenderContentMenuOption.bind(this);
    }


    componentWillMount() {
        this.setState({
            contextMenu: [],
            resultCount: 0,
            renderContextMenuIndex: null
        });
    }


    download(FileID) {

        var nameuri = 'api/enCapsaAPI/GetFormData/';

        var URL = nameuri + 'UPLOADFILE/' + fileID


        alert(URL);


        var xmlhttp = new XMLHttpRequest();

        xmlhttp.open("GET", URL, false);

        //  var x = localStorage.getItem("token");

        xmlhttp.setRequestHeader('Authorization', 'Bearer ' + JSON.parse(localStorage.getItem("token")));

        xmlhttp.send();

        if (xmlhttp.status == "200") {

            alert("OK");


        } else {


            alert(xmlhttp.status);


        }

    }

    parseSearchResults(searchResultsString) {
        console.log('this.parseSearchResults normal')
        const searchResults = JSON.parse(searchResultsString);
        console.log(searchResults);
        console.log(searchResults.Row);
        let tableData = [];
        if (searchResults && searchResults.Row) {
            console.log(searchResults.Row);
            searchResults.Row.forEach(function (rowItem) {
                let tableItem = {};
                rowItem.SearchResultItems && rowItem.SearchResultItems.forEach(function (item) {
                    switch (item.FieldName) {
                        case 'FILEID':
                        case 'FULLFILENAME':
                        case 'CONTENTTYPE':
                        case 'FILENAME':
                        case '_FILENAME':
                        case 'FOLDERNAME':
                        case 'FILESIZE':
                        case '_FILESIZE':
                        case 'FILEEXT':
                        case '_FILEEXT':
                        case 'LASTMODIFIEDDATE':
                        case 'DATEADDED':
                            tableItem[item.FieldName] = item.Data;
                    }
                });
                console.log('Table Item');
                console.log(tableItem);
                tableData.push(tableItem);
            });
        }
        return tableData;
    }

    parseSearchResultsWithRow(searchResultsString) {
        console.log('this.parseSearchResults Row')
        console.log(searchResultsString);
        const searchResults = JSON.parse(searchResultsString);
        console.log(searchResults);
        console.log(searchResults.Row);
        let tableData = {
            Row: []
        };
        if (searchResults && searchResults.Row) {
            console.log(searchResults.Row);
            searchResults.Row.forEach(function (rowItem) {
                let tableItem = {};
                rowItem.SearchResultItems && rowItem.SearchResultItems.forEach(function (item) {
                    switch (item.FieldName) {
                        case 'FILEID':
                        case 'FULLFILENAME':
                        case 'CONTENTTYPE':
                        case 'FILENAME':
                        case '_FILENAME':
                        case 'FOLDERNAME':
                        case 'FILESIZE':
                        case '_FILESIZE':
                        case 'FILEEXT':
                        case '_FILEEXT':
                        case 'LASTMODIFIEDDATE':
                        case 'DATEADDED':
                            tableItem[item.FieldName] = item.Data;
                    }
                });
                console.log('Table Item');
                console.log(tableItem);
                tableData.Row.push(tableItem);
            });
        }
        return tableData;
    }

    formatBytes(a, b) {
        if (a) {
            const bytes = parseInt(a);
            if (0 == bytes) return "0 Bytes";
            var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                f = Math.floor(Math.log(bytes) / Math.log(c));
            return parseFloat((bytes / Math.pow(c, f)).toFixed(d)) + " " + e[f];
        } else {
            return '';
        }
    }

    generateIconByFileExt(fileExt,folderName) {
        console.log('generate icon');
        console.log(fileExt);

        if(folderName) {
            return (<img src={StakGreen}/>);
        }

        switch (fileExt) {
            case 'png':
                return (<img src={Png}/>);
            case 'jpg':
            case 'jpeg':
                return (<img src={Jpeg}/>);
            case '.pdf':
            case 'pdf':
                return (<img src={AdobeFile}/>);
            case '.txt':
            case 'txt':
                return (<img src={Txt}/>);
            case '.docx':
            case 'docx':
            case 'doc':
                return (<img src={WordFile}/>);
            default:
                return (<img src={RandomFile}/>);
        }
    }

    renderContextMenu(index, tableItem) {

        if (this.state.renderContextMenuIndex != index) {
            return null;
        }

        return (<div className="context-menu-container">
            <div className='context-menu-block'> Preview |</div>
            <div className='context-menu-block add-to-stak' onClick={() => {this.props.showNewStakModal(tableItem)}}> Add To Stak |</div>
            <div className='context-menu-block'> Download </div>
        </div>);
    }

    setRenderContentMenuOption(index) {
        this.setState({
            renderContextMenuIndex: index
        });
    }

    render() {
        let tableData = {
            Row: []
        };

        if(this.props.searchResults && this.props.searchResults !== 'NOT FOUND') {
            tableData = this.parseSearchResultsWithRow(this.props.searchResults);
        }

        console.log(tableData);

        const data = {
            header: {
                iconClass: {
                    list: 'pull-right-88',
                    grid: ''
                },
                list: [
                    {label: 'Name', className: 'home-folder-inline-position div-name'},
                    {label: 'Modified', className: 'home-folder-inline-position div-modified'},
                    {label: 'Date Added', className: 'home-folder-inline-position div-date-added'},
                    {label: 'File Size', className: 'home-folder-inline-position div-file-size'}
                ],
                grid: [{label:   'Name', className: 'home-folder-inline-position div-name'}]
            },
            fileData: tableData,
            options: {
                defaultLayout: 'list',
                enableContextMenu: true
            }
        };

        const searchResults = tableData.Row.length;

        return (
            <div className='div-home-folder-container'>
                <div className='home-folder-content div-file-result-content'>
                    <div className='div-result-title'>
                        <label>{searchResults} results</label>
                    </div>
                    <FileDisplayLayout data={data} showNewStakModal={this.props.showNewStakModal}/>
                </div>
                <div className="div-modal-footer-button-group div-stak-footer-button-group">
                    <button className="rounded-button-green-bg"
                            onClick={this.props.showSaveSearchModal}> Save
                    </button>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    console.log('STATES');
    console.log(state);
    return {
        searchQuery: state.search ? state.search.searchQuery : '',
        searchResults: state.search ? state.search.searchResults : ''
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({showNewStakModal: showNewStakModal, showSaveSearchModal: showSaveSearchModal}, dispatch);
}
const SearchResultsContainer = connect(mapStateToProps, mapDispatchToProps)(SearchResults);

export default SearchResultsContainer;