import React, {Component} from 'react';
import {connect} from 'react-redux';
import {saveData} from './../services/service';
import {bindActionCreators} from 'redux';

class Upload extends Component {

    constructor(props) {
        super(props);

        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.uploadFiles = this.uploadFiles.bind(this);
    }

    componentWillMount() {

        this.setState({
            files: []
        })
    }

    handleFileUpload(event) {
        console.log(event.target.files);
        this.setState({
            files: event.target.files
        })
    }

    uploadFiles() {
        console.log(this.state.files);
        if(this.state.files) {
            saveData(this.state.files).then(function(response) {
                console.log("Successfully Uploaded Files");
            }).catch(function(error) {
                console.log("Error uploading Files");
            });
        }
    }

    render() {
        let searchTitle = 'Search: ';
        return (
            <div className='div-upload'>
                <div className="flexbox-container">
                    <div className="search-message">
                        <p>{searchTitle}</p>
                    </div>
                    <form id='file-upload' onSubmit={this.uploadFiles}>
                        <input type="file" type="file" name="filename" onChange={this.handleFileUpload}/>
                        <input type="submit" value="submit" />
                    </form>
                </div>
                <hr/>
            </div>

    )
        ;
    }
}

const mapStateToProps = (state) => {
    return {
        searchQuery: state.search ? state.search.searchQuery : ''
    };
}

const mapDispatchToProps = (dispatch) => {
}

const UploadContainer = connect(mapStateToProps, mapDispatchToProps)(Upload);

export default UploadContainer;