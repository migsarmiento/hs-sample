import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    hideSaveSearchModal
} from "../redux-store/actions/modal-actions";
import {
    setSavedSearchItems
} from "../redux-store/actions/search-actions"
import {saveSearchQuery,loadTableData} from "../services/service";

class SaveSearchModal extends Component {

    constructor(props) {
        super(props);
        this.submitNewStackModalForm = this.submitNewStackModalForm.bind(this);
        this.onSelectStak = this.onSelectStak.bind(this);
    }

    componentWillMount() {
        this.setState({
            searchResults: [],
            selectedStak: {}
        });

        const tableData = loadTableData('DOCUMENTFOLDER');
        tableData.then((response) => {
            const searchResults = JSON.parse(response.data.replace(/\\\//g, "/"));
            this.setState({
                searchResults: searchResults.Row
            });
        }).catch( (error) => {
            this.setState({
                searchResults: []
            });
        });
    }

    submitNewStackModalForm() {
        const setSavedSearchItems = this.props.setSavedSearchItems;
        const promise = loadTableData('SAVEDSEARCHESFINAL');
        let latestWorkerId = '1';
        promise.then( (response) => {
            const savedSearches = JSON.parse(response.data.replace(/\\\//g, "/"));
            console.log(savedSearches.Row);
            const searchResults = savedSearches.Row;
            if(searchResults && searchResults.length > 0) {
                console.log('Appendning worker id');
                console.log(searchResults);
                console.log(searchResults[0]._WORKERID);
                latestWorkerId = searchResults[0]._WORKERID;
            } else {
                latestWorkerId = '1';
            }
            const xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", "http://10.10.1.202/api/enCapsaGlobalAPI/Submit/FormData", false);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            console.log('WORKER ID');
            console.log(latestWorkerId);
            console.log('parse int' + parseInt(latestWorkerId, 10));
            const newWorkerId = parseInt(latestWorkerId, 10) + 1;

            const params = {
                _USERID: 'GLOBAL',
                _TABLENAME: "SAVEDSEARCHESFINAL",
                _WORKERID: newWorkerId,
                _SAVEDSEARCH: this.props.searchQuery,
            }

            xmlhttp.onreadystatechange = function () {

                if (xmlhttp.readyState == XMLHttpRequest.DONE) {

                    if (xmlhttp.status == 200) {
                        const promise = loadTableData('SAVEDSEARCHESFINAL');
                        promise.then( (innerResponse) => {
                            const savedSearches = JSON.parse(innerResponse.data.replace(/\\\//g, "/"));
                            console.log(savedSearches.Row);
                            const searchData = savedSearches.Row;
                            setSavedSearchItems(searchData);
                            alert("successfully saved search");
                        }).catch((error) => {
                            alert("Something went wrong. Upload failed.");
                        });

                    } else {
                        alert("Something went wrong. Upload Failed.");
                    }

                }
            }

            xmlhttp.send(Object.keys(params).map(key => key + '=' + encodeURIComponent(params[key])).join('&'));

        });
        this.props.hideSaveSearchModal();



        /*saveSearchQuery({dateUploaded: '1/1/2018', positionNumber: '1', recordId: '1', searchQuery: this.props.searchQuery}).then( (response) => {
            alert("Search Save Successful.");
            const promise = loadTableData('SAVEDSEARCHES');
            promise.then( (innerResponse) => {
                const savedSearches = JSON.parse(innerResponse.data.replace(/\\\//g, "/"));
                console.log(savedSearches.Row);
                const searchData = savedSearches.Row;
                setSavedSearchItems(searchData);
            }).catch((error) => {
                alert("Something went wrong. Upload failed.");
            });
        });*/
    }

    onSelectStak(event) {
        const selectedStak = event.target.value;
        console.log('SELECTED STAK');
        console.log(selectedStak);
        this.setState({
            selectedStak: selectedStak
        });
    }

    render() {
        const content = this.props.searchQuery ? this.props.searchQuery : '';
        console.log('stak modal');
        console.log(this.state);
        if (this.props.displayModal) {
            return (
                <div className="div-modal-backdrop-container">
                    <div className="div-modal-container div-stak-modal">
                        <div className="div-modal-content">
                            <div className="div-modal-header">
                                <label>Save Search</label>
                                <button onClick={this.props.hideSaveSearchModal} className='div-stak-close'>Close X</button>
                            </div>
                            <div className="div-modal-body div-stak-body">
                                <div className="div-new-stak-container div-stak-container">
                                    <div className="div-new-stak-row">
                                        <div className="div-row-left-side position-inline-class">
                                            <label>Save Search:</label>
                                        </div>
                                        <div className="div-row-right-side position-inline-class">
                                            <input className='div-stak-input' type='text' placeholder={content} disabled/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="div-modal-footer div-stak-footer">
                                <div className="div-modal-footer-button-group div-stak-footer-button-group">
                                    <button className="rounded-button-white-bg"
                                            onClick={this.props.hideSaveSearchModal}> Cancel
                                    </button>
                                    <button className="rounded-button-green-bg"
                                            onClick={this.submitNewStackModalForm}> Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }

    }

}

const mapStateToProps = (state) => {
    return {
        searchQuery: state.search ? state.search.searchQuery : '',
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        hideSaveSearchModal,
        setSavedSearchItems: setSavedSearchItems
    }, dispatch);
}

const SaveSearchModalContainer = connect(mapStateToProps, mapDispatchToProps)(SaveSearchModal);

export default SaveSearchModalContainer;