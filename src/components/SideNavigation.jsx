import React, {Component} from 'react';

import {connect} from 'react-redux';
import haystack from './../styles/assets/haystak-logo.png';
import User from './../styles/assets/User.png';
import UserGreen from './../styles/assets/User Green.png';
import Home from './../styles/assets/Home.png';
import HomeGreen from './../styles/assets/Home Green.png'
import FilesGray from './../styles/assets/Files Gray.png';
import FilesGreen from './../styles/assets/Files Green.png';
import StakGray from './../styles/assets/Stak Gray.png';
import StakGreen from './../styles/assets/Stak Green.png';
import SearchGray from './../styles/assets/Search Gray.png';
import SearchGreen from './../styles/assets/Search Green.png';
import {setSearchQuery, setSearchResults, setSavedSearchItems} from './../redux-store/actions/search-actions';
import {setNavPage} from './../redux-store/actions/nav-page-actions';
import {bindActionCreators} from 'redux';
import {loadTableData, search} from "../services/service";

class SideNavigation extends Component {

    constructor(props) {
        super(props);
        this.removeSearchQuery = this.removeSearchQuery.bind(this);
        this.navigateToHome = this.navigateToHome.bind(this);
        this.navigateToProfile = this.navigateToProfile.bind(this);
        this.navigateToStaks = this.navigateToStaks.bind(this);
        this.navigateToFiles = this.navigateToFiles.bind(this);
        this.navigateToLoginPage = this.navigateToLoginPage.bind(this);
        this.renderSavedSearches = this.renderSavedSearches.bind(this);
        this.renderRecentSearches = this.renderRecentSearches.bind(this);
        this.search = this.search.bind(this);
        this.toggleRecentSearches = this.toggleRecentSearches.bind(this);
        this.toggleSavedSearches = this.toggleSavedSearches.bind(this);
    }

    componentWillMount() {

        const promise = loadTableData('SAVEDSEARCHESFINAL');
        promise.then((response) => {
            const savedSearches = JSON.parse(response.data.replace(/\\\//g, "/"));
            console.log(savedSearches.Row);
            const searchData = savedSearches.Row;
            this.props.setSavedSearchItems(searchData);
        });

        this.setState({
            selectedMenu: 'Files',
            showSavedSearches: true,
            showRecentSearches: true,
        });
    }

    temp() {
        /*<div className="side-nav-labels">
            <label> Refine by: </label>
        </div>

        <ul className="ul-list-02">
            <li>
            <input type="radio"/>
            <label> Folder Name </label>
    </li>
        <li>
            <input type="radio"/>
            <label> Project Name </label>
        </li>
        <li>
            <input type="radio"/>
            <label> Date </label>
        </li>
        <li>
            <input type="radio"/>
            <label> Author </label>
        </li>
        <li>
            <label> <a>More...</a> </label>
            </li>
        </ul>*/
    }

    removeSearchQuery() {
        this.props.setSearchQuery('');
    }

    navigateToHome() {
        this.props.setSearchQuery('');
        this.props.setNavPage('Home');
        this.setState({
            selectedMenu: 'Home'
        })
    }

    navigateToProfile() {
        this.props.setSearchQuery('');
        this.props.setNavPage('Profile');
        this.setState({
            selectedMenu: 'Profile'
        })
    }

    navigateToStaks() {
        this.props.setSearchQuery('');
        this.props.setNavPage('Stak');
        this.setState({
            selectedMenu: 'Stak'
        })
    }

    navigateToFiles() {
        this.props.setSearchQuery('');
        this.props.setNavPage('Files');
        this.setState({
            selectedMenu: 'Files'
        })
    }

    navigateToLoginPage() {
        this.props.setSearchQuery('');
        this.props.setNavPage('');
        this.setState({
            selectedMenu: 'Home'
        })
    }

    search(searchQuery) {
        this.props.setSearchQuery(searchQuery);
        search(searchQuery).then((response) => {
            console.log('SUCCESSFULLY RETRIEVED DATA');
            this.props.setSearchResults(response.data);
        }).catch((error) => {
            console.log('ERROR FETCHING DATA');
            console.log(error);
            this.props.setSearchResults(0);
        });
    }

    toggleSavedSearches() {
        this.setState({
            showSavedSearches: !this.state.showSavedSearches
        });
    }

    renderSavedSearches() {
        if (this.props.savedSearchQueries) {
            return (<div className='saved-searches-nav'>
                <div className='saved-searches-header' onClick={this.toggleSavedSearches}>
                    Saved Searches
                </div>
                <hr/>
                {
                    this.state.showSavedSearches && this.props.savedSearchQueries.map((savedSearchQuery, index) => {
                        if (index < 5) {
                            return (<div className='saved-searches'
                                         onClick={() => this.search(savedSearchQuery._SAVEDSEARCH)}>
                                    <img className="side-navigation-searched-img" src={SearchGreen}/>
                                    <label className="side-navigation-searched-label">{savedSearchQuery._SAVEDSEARCH}</label>
                                    <hr/>
                                </div>
                            );
                        }
                    })
                }
            </div>);
        }
    }

    toggleRecentSearches() {
        this.setState({
            showRecentSearches: !this.state.showRecentSearches
        });
    }

    renderRecentSearches() {
        if (this.props.recentSearchQueries) {
            return (<div className='recent-searches-nav'>
                <div className='recent-searches-header' onClick={this.toggleRecentSearches}>
                    Recent Searches
                </div>
                <hr/>
                {
                    this.state.showRecentSearches && this.props.recentSearchQueries.map((recentSearchQuery) => {
                        return (<div className='recent-searches' onClick={() => this.search(recentSearchQuery)}>
                                <img className="side-navigation-searched-img" src={SearchGray}/>
                                <label className="side-navigation-searched-label">{recentSearchQuery}</label>
                                <hr/>
                            </div>
                        );
                    })
                }
            </div>);
        }
    }

/*<div className='nav-button' id='home-nav-button' onClick={this.navigateToHome}>
                        <img src={(this.state.selectedMenu && this.state.selectedMenu === 'Home') ? HomeGreen : Home}/>
                        <label>Home</label>
                    </div>
                    <br/>*/
    render() {
        return (
            <div className='div-side-navigation'>
                <div className="side-nav-logo-container">
                    <div className="side-nav-logo">
                        <img src={haystack}/>
                    </div>
                </div>

                <div className="side-nav-labels">

                    <div className='nav-button' id='profile-nav-button' onClick={this.navigateToProfile}>
                        <img
                            class="nav-img-logos side-navigation-img"
                            src={(this.state.selectedMenu && this.state.selectedMenu === 'Profile') ? UserGreen : User}
                        />
                        <label className="side-navigation-label">Profile</label>
                    </div>
                    <br/>



                    <div className='nav-button' id='files-nav-button' onClick={this.navigateToFiles}>
                        <img
                            class="nav-img-logos side-navigation-img"
                            src={(this.state.selectedMenu && this.state.selectedMenu === 'Files') ? FilesGreen : FilesGray}
                        />
                        <label className="side-navigation-label">Files</label>
                    </div>
                    <br/>

                    <div className='nav-button' id='staks-nav-button' onClick={this.navigateToStaks}>
                        <img
                            class="nav-img-logos side-navigation-img"
                            src={(this.state.selectedMenu && this.state.selectedMenu === 'Stak') ? StakGreen : StakGray}
                        />
                        <label className="side-navigation-label">Staks</label>
                    </div>

                    <div className='searches'>
                        {this.renderSavedSearches()}
                        {this.renderRecentSearches()}
                    </div>

                    <button className="side-nav-logout pull-right btn-primary" onClick={this.navigateToLoginPage}>LOG
                        OUT
                    </button>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log('Side bar state');
    console.log(state);
    return {
        searchQuery: state.search ? state.search.searchQuery : '',
        navPage: state.navPage ? state.navPage.navPage : '',
        savedSearchQueries: state.search ? state.search.savedSearchItems : [],
        recentSearchQueries: state.search ? state.search.recentSearchedItems : [],
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setSearchQuery: setSearchQuery,
        setSearchResults: setSearchResults,
        setSavedSearchItems: setSavedSearchItems,
        setNavPage: setNavPage,
    }, dispatch);
}

const SideNavigationContainer = connect(mapStateToProps, mapDispatchToProps)(SideNavigation);

export default SideNavigationContainer;