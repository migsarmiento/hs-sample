import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    hideConnectModal
} from "../redux-store/actions/modal-actions";

class ConnectModal extends Component {

    constructor(props) {
        super(props);
        this.submitConnectModalForm = this.submitConnectModalForm.bind(this);
        this.fileInputHandler = this.fileInputHandler.bind(this);
        this.setUploadType = this.setUploadType.bind(this);
        this.onSubmitFileUpload = this.onSubmitFileUpload.bind(this);

        this.showFileChooser = this.showFileChooser.bind(this);

        this.displayTabPanes = this.displayTabPanes.bind(this);
        this.doTestConnection = this.doTestConnection.bind(this);

        this.uploadType = {
            UPLOAD_FILE: 'UPLOAD_FILE',
            NETWORK_DRIVE: 'NETWORK_DRIVE',
            DATABASE: 'DATABASE'
        };

        this.isValidConnect = false;
    }

    componentWillMount() {

        this.fileUploadSideInput = React.createRef();
        this.fileUploadFileInput = React.createRef();

        this.setState({
            files: '',
            selectedUploadType: this.uploadType.UPLOAD_FILE
        });
    }

    showFileChooser(event) {
        event.preventDefault();
        this.fileUploadFileInput.current.click();
    }

    fileInputHandler(event) {
        event.preventDefault();
        const files = event.target.files;
        const file = files[0];
        this.setState({
            file: file
        });

        this.fileUploadSideInput.current.value = file.name;
    }

    onSubmitFileUpload(event) {

        const file = this.state.file;

        if(file) {

            const reader = new FileReader();

            reader.readAsDataURL(file);
            console.log('FILE DATA');
            console.log(file);
            reader.onload = () => {
                const params = {
                    _USERID: "GLOBAL",
                    _TABLENAME: "FILEUPLOAD",
                    _FILEDATA: reader.result,
                    _FILENAME: file.name,
                    _FILEEXT: file.name.substring(file.name.indexOf('.') + 1, file.name.length),
                    _FILESIZE: file.size,
                    _CONTENTTYPE: file.type,
                    _TIMESTAMP: file.lastModifiedDate
                }

                const xmlhttp = new XMLHttpRequest();
                xmlhttp.open("POST", "http://10.10.1.202/api/Post/PostFile/FormData", false);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImNjb2tlckBlbmNhcHNhLmNvbSIsIm5iZiI6MTUzNDA1ODE1MSwiZXhwIjoxNTM0NjYyOTUxLCJpYXQiOjE1MzQwNTgxNTEsImlzcyI6Imh0dHA6Ly8xMC4xMC4xLjIwMTo4MCIsImF1ZCI6Imh0dHA6Ly8xMC4xMC4xLjIwMTo4MCJ9.NbIQMhNVmv28kqpJIcznt-aoZBKK8D2r9UPNPz_wlDs");

                /*xmlhttp.open("POST", "http://10.10.1.201/api/Upload/PostFile/FormData", false);

                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");*/

                xmlhttp.onreadystatechange = function () {

                    if (xmlhttp.readyState == XMLHttpRequest.DONE) {

                        if (xmlhttp.status == 200) {
                            alert("Upload Successful.");
                        } else {
                            alert("Something went wrong. Upload Failed.");
                        }

                    }
                }

                xmlhttp.send(Object.keys(params).map(key => key + '=' + encodeURIComponent(params[key])).join('&'));
            }

        }
    }

    submitConnectModalForm(event) {
        event.preventDefault();

        const uploadType = this.state.selectedUploadType;

        if (uploadType) {

            switch (uploadType) {
                case this.uploadType.UPLOAD_FILE:
                    this.onSubmitFileUpload(event);
                    break;
                case this.uploadType.NETWORK_DRIVE:
                    break;
                case this.uploadType.DATABASE:
                    break;
            }

            this.props.hideConnectModal();

        }

    }

    doTestConnection(event) {

        event.preventDefault();

        alert("Implement me: Test database connection");
    }

    setUploadType(event) {

        event.preventDefault();

        this.setState({
            // selectedUploadType: event.target.value
            selectedUploadType: event.target.getAttribute("data-value")
        });
    }

    displayTabPanes() {

        if (this.state.selectedUploadType === this.uploadType.UPLOAD_FILE) {
            return this.displayUploadTab();                  
        } else if (this.state.selectedUploadType === this.uploadType.DATABASE) {
            return this.displayConnectToDatabaseTab();
        } else if (this.state.selectedUploadType === this.uploadType.NETWORK_DRIVE) {
            return this.displayConnectToShareTab();
        } else {
            return null;
        }

    }

    displayUploadTab() {
        return (
            <table>
                <tr>
                    <td className="width-of-first-table-column-02">
                        <label className="label-font-02">Browse File</label>
                    </td>
                    <td colSpan="2">
                        <input ref={this.fileUploadFileInput} type="file" onChange={this.fileInputHandler} />
                        <input ref={this.fileUploadSideInput} className="file-input-text-upload" type="text" disabled/>
                        <button className="connect-select-destination-button" onClick={this.showFileChooser} >Select Destination</button>
                    </td>
                </tr>
            </table>
        );
    }

    displayConnectToShareTab() {
        return (
            <table>

                <tr>
                    <td className="width-of-first-table-column-02">
                        <label className="label-font-02">Server Type</label>
                    </td>

                    <td colSpan="2">
                        <select>
                            <option>SMB</option>
                            <option>NTFS</option>
                            <option>Dropbox</option>
                            <option>Box.net</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td className="width-of-first-table-column-02">
                        <label className="label-font-02">Address</label>
                    </td>

                    <td colSpan="2">
                        <input type="text"/>
                        <button className="connect-to-share-button">Browse</button>
                    </td>
                </tr>

                <tr>
                    <td className="width-of-first-table-column"></td>
                    <td colSpan="2">
                        <label className="label-font-02">Username</label>
                        <input className="input-field-for-authentications" type="text"/>
                    </td>
                </tr>

                    <tr>
                    <td className="width-of-first-table-column"></td>
                    <td colSpan="2">
                        <label className="label-font-02">Password</label>
                        <input className="input-field-for-authentications" type="password" />
                    </td>
                </tr>
            </table>
        );
    }

    displayConnectToDatabaseTab() {
        return (
            <table>
                <tr>
                    <td className="width-of-first-table-column-02">
                        <label className="label-font-02">Server Type</label>
                    </td>
                    <td colSpan="2">
                        <select>
                            <option>Microsoft SQL Server</option>
                            <option>MySQL</option>
                            <option>Oracle</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td className="width-of-first-table-column-02">
                        <label className="label-font-02">Host</label>
                    </td>
                    <td colSpan="2">
                        <input className="input-field-for-db-host" type="text" />
                    </td>

                </tr>

                <tr>
                    <td className="width-of-first-table-column-02">
                        <label className="label-font-02">Authentication</label>
                    </td>
                    <td colSpan="2">
                        <input className="input-field-for-db-authentication" type="text" />
                    </td>

                </tr>

                <tr>
                    <td className="width-of-first-table-column"></td>
                    <td colSpan="2">
                        <label className="label-font-02">Username</label>
                        <input className="input-field-for-authentications" type="text"/>
                    </td>
                </tr>

                <tr>
                    <td className="width-of-first-table-column"></td>
                    <td colSpan="2">
                        <label className="label-font-02">Password</label>
                        <input className="input-field-for-authentications" type="password"/>
                        <button className="connect-test-db-button" onClick={this.doTestConnection}>Test</button>
                    </td>
                </tr>
            </table>
        );
    }

    render() {
        if (this.props.displayModal) {
            return (
                <div className="div-modal-backdrop-container">
                    <div className="div-modal-container-2">
                        <div className="div-modal-content">
                            <div className="div-modal-header">
                                <label>Connect</label>
                                <button className="connect-close-button" onClick={this.props.hideConnectModal}>X</button>
                            </div>
                            <form>
                                <div className="div-modal-body">

                                    <div className="div-connect-modal-container">
                                        <ul className="nav tabs">
                                            <li className="nav-item tab"><a className="nav-link active" href="#" onClick={this.setUploadType} data-value={this.uploadType.UPLOAD_FILE}>Upload File</a></li>
                                            <li className="nav-item tab"><a className="nav-link" href="#" onClick={this.setUploadType} data-value={this.uploadType.NETWORK_DRIVE}>Connect to Share</a></li>
                                            <li className="nav-item tab"><a className="nav-link" href="#" onClick={this.setUploadType} data-value={this.uploadType.DATABASE}>Connect to Database</a></li>
                                        </ul>

                                        <div className="pane">
                                            {this.displayTabPanes()}
                                        </div>

                                        {/* <table>
                                            <tr>
                                                <td className="width-of-first-table-column">
                                                    <input type="radio" name="uploadType" value={this.uploadType.UPLOAD_FILE} onClick={this.setUploadType} defaultChecked disabled={this.state.selectedUploadType != this.uploadType.UPLOAD_FILE}/>
                                                    <label className="label-font-01">Upload File</label>
                                                </td>
                                                <td className="width-of-first-table-column-02">
                                                    <label className="label-font-02">Browse File</label>
                                                </td>
                                                <td colSpan="2">
                                                    <input ref={this.fileUploadFileInput} type="file" onChange={this.fileInputHandler} disabled={this.state.selectedUploadType != this.uploadType.UPLOAD_FILE}/>
                                                    <input ref={this.fileUploadSideInput} className="file-input-text" type="text" disabled/>
                                                    <button className="connect-to-share-button" onClick={this.showFileChooser} disabled={this.state.selectedUploadType != this.uploadType.UPLOAD_FILE}>...</button>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td className="width-of-first-table-column">
                                                    <input type="radio" name="uploadType" value={this.uploadType.NETWORK_DRIVE} onClick={this.setUploadType} />
                                                    <label className="label-font-01">Connect to Share</label>
                                                </td>

                                                <td className="width-of-first-table-column-02">
                                                    <label className="label-font-02">Server Address</label>
                                                </td>

                                                <td colSpan="2">
                                                    <input type="text" disabled={this.state.selectedUploadType != this.uploadType.NETWORK_DRIVE}/>
                                                    <button className="connect-to-share-button" disabled={this.state.selectedUploadType != this.uploadType.NETWORK_DRIVE}>+</button>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td className="width-of-first-table-column"></td>
                                                
                                                <td className="width-of-first-table-column-02">
                                                    <label className="label-font-02">Server Type</label>
                                                </td>

                                                <td colSpan="2">
                                                    <select disabled={this.state.selectedUploadType != this.uploadType.NETWORK_DRIVE}>
                                                        <option>SMB</option>
                                                        <option>NTFS</option>
                                                        <option>Dropbox</option>
                                                        <option>Box.net</option>
                                                    </select>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td className="width-of-first-table-column"></td>
                                                <td></td>
                                                <td colSpan="2">
                                                    <label className="label-font-02">Username</label>
                                                    <input className="input-field-for-authentications" type="text" disabled={this.state.selectedUploadType != this.uploadType.NETWORK_DRIVE}/>
                                                </td>
                                            </tr>

                                             <tr>
                                                <td className="width-of-first-table-column"></td>
                                                <td></td>
                                                <td colSpan="2">
                                                    <label className="label-font-02">Password</label>
                                                    <input className="input-field-for-authentications" type="text" disabled={this.state.selectedUploadType != this.uploadType.NETWORK_DRIVE} />
                                                </td>
                                            </tr>

                                            <tr>
                                                <td className="width-of-first-table-column">
                                                    <input type="radio" name="uploadType" value={this.uploadType.DATABASE} onClick={this.setUploadType} />
                                                    <label className="label-font-01">Connect to Database</label>
                                                </td>
                                                <td className="width-of-first-table-column-02">
                                                    <label className="label-font-02">Server Type</label>
                                                </td>
                                                <td colSpan="2">
                                                    <select disabled={this.state.selectedUploadType != this.uploadType.DATABASE}>
                                                        <option>Microsoft SQL Server</option>
                                                        <option>MySQL</option>
                                                        <option>Oracle</option>
                                                    </select>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td className="width-of-first-table-column"></td>
                                                <td className="width-of-first-table-column-02">
                                                    <label className="label-font-02">Host Name/IP</label>
                                                </td>
                                                <td colSpan="2">
                                                    <select disabled={this.state.selectedUploadType != this.uploadType.DATABASE}>
                                                        <option></option>
                                                        <option>option server 1</option>
                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td className="width-of-first-table-column"></td>
                                                <td className="width-of-first-table-column-02">
                                                    <label className="label-font-02">Authentication</label>
                                                </td>
                                                <td colSpan="2">
                                                   <select disabled={this.state.selectedUploadType != this.uploadType.DATABASE}>
                                                        <option></option>
                                                        <option>option server 1</option>
                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td className="width-of-first-table-column"></td>
                                                <td></td>
                                                <td colSpan="2">
                                                    <label className="label-font-02">Username</label>
                                                    <input className="input-field-for-authentications" type="text" disabled={this.state.selectedUploadType != this.uploadType.DATABASE}/>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td className="width-of-first-table-column"></td>
                                                <td></td>
                                                <td colSpan="2">
                                                    <label className="label-font-02">Password</label>
                                                    <input className="input-field-for-authentications" type="text" disabled={this.state.selectedUploadType != this.uploadType.DATABASE}/>
                                                </td>
                                            </tr>

                                        </table> */}

                                    </div>

                                </div>
                                <div className="div-modal-footer">
                                    <div className="div-modal-footer-button-group">
                                        <button className="rounded-button-green-bg" onClick={this.submitConnectModalForm}> Connect</button>
                                        {/* <button className="rounded-button-white-bg" onClick={this.props.hideConnectModal}> Cancel</button> */}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }

}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        hideConnectModal
    }, dispatch);
}

const ConnectModalContainer = connect(null, mapDispatchToProps)(ConnectModal);

export default ConnectModalContainer;

/*<form id='file-upload' onSubmit={this.uploadFiles}>
    <input id="file-input" ref={this.fileUpload} type="file" name="filename"
           onChange={this.handleFileUpload} hidden/>
    <button id='submit-button' ref={this.formSubmit} value="Submit" hidden/>
</form>*/