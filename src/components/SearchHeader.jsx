import React, {Component} from 'react';
import {connect} from 'react-redux';
import Magnifying from './../styles/assets/Magnifying.png';
import {search} from './../services/service';
import {setSearchQuery, setSearchResults} from './../redux-store/actions/search-actions';
import {bindActionCreators} from 'redux';
import {setRecentSearchedItems} from "../redux-store/actions/search-actions";



class SearchHeader extends Component {

    constructor(props) {
        super(props);
        this.setSearchInput = this.setSearchInput.bind(this);
        this.search = this.search.bind(this);
        this.toggleSearch = this.toggleSearch.bind(this);
        this.toggleSearchOnEnter = this.toggleSearchOnEnter.bind(this);
    }

    componentWillMount() {
        this.setState({
            initialState: '',
            textInput: '',
            defaultTextInput: '',
        })
    }

    componentWillReceiveProps(nextProps) {
        console.log('magic');
        console.log(nextProps.searchQuery);
        console.log(this.props.searchQuery);
    }

    setSearchInput(event) {
        this.setState({
            textInput: event.target.value
        });
    }

    search(searchQuery) {
        const params = {
            _SEARCHID: searchQuery
        };
        const setSearchResults = this.props.setSearchResults;

        const xmlHttp = new XMLHttpRequest();
        xmlHttp.open('POST','http://10.10.1.202/api/Search/PostSearch/FormData', false);
        xmlHttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlHttp.onreadystatechange = function() {
            if(xmlHttp.readyState == XMLHttpRequest.DONE) {
                if(xmlHttp.status == 200) {
                    const response = xmlHttp.responseText;
                    console.log('SUCCESS');
                    console.log(response);
                    if(response) {
                        setSearchResults(JSON.parse(response.replace(/\\\//g, "/")));
                    } else {
                        setSearchResults('');
                    }
                } else {
                    setSearchResults('');
                }
            }

        }
        xmlHttp.send(Object.keys(params).map(key => key + '=' + encodeURIComponent(params[key])).join('&'));
    }

    toggleSearch() {
        console.log(this.state);
        if (this.state.textInput) {
            this.props.setSearchQuery(this.state.textInput);

            let recentSearchedItems = this.props.recentSearchedItems;
            console.log('recent searched items');
            console.log(recentSearchedItems);
            if(recentSearchedItems) {
                recentSearchedItems.push(this.state.textInput);

                if(recentSearchedItems.length > 5) {
                    recentSearchedItems = recentSearchedItems.slice(1);
                }

            } else {
                recentSearchedItems = [];
                recentSearchedItems.push(this.state.textInput);
            }

            this.props.setRecentSearchedItems(recentSearchedItems);
            this.search(this.state.textInput);
        }
    }

    toggleSearchOnEnter(event) {
        if(event.key === 'Enter') {
            this.toggleSearch();
        }
    }

    render() {
        return (
            <div className='searchHeader'>
                <input type='text' placeholder={'Search your haystak drive'} defaultValue={this.props.searchQuery} onChange={this.setSearchInput} onKeyPress={this.toggleSearchOnEnter}/>
                <i class="searchIcon"><img src={Magnifying} onClick={this.toggleSearch}/></i>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log('STATES');
    console.log(state);
    return {
        searchQuery: state.search ? state.search.searchQuery : '',
        recentSearchedItems: state.search ? state.search.recentSearchedItems : [],
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setSearchQuery: setSearchQuery,
        setSearchResults: setSearchResults,
        setRecentSearchedItems: setRecentSearchedItems
    }, dispatch);
}

const SearchHeaderContainer = connect(mapStateToProps, mapDispatchToProps)(SearchHeader);

export default SearchHeaderContainer;