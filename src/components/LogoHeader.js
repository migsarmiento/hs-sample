import React, {Component} from 'react';
import HaystakLogo from './../styles/assets/haystak-logo.png';

class LogoHeader extends Component {

    render() {
        return (
            <div className="div-logo-header">
                <div className="home-folder-inline-position logo-header-container">
                    <img className="logo-header" src={HaystakLogo} />
                </div>

                <div className="div-faqs-help">

                    <div className="home-folder-inline-position anchor-tag">
                        <a href="">FAQs</a>
                    </div>

                    <div className="home-folder-inline-position anchor-tag">
                        <a href=""><label>Need_Help?</label></a>
                    </div>

                </div>

            </div>
        );
    }
}

export default LogoHeader;