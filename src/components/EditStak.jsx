import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    hideEditStakModal
} from "../redux-store/actions/modal-actions";
import {loadTableData} from "../services/service";
import {setNavPage} from "../redux-store/actions/nav-page-actions";
import SearchModal from "./SearchModal";

class EditStakModal extends Component {

    constructor(props) {
        super(props);
        this.returnToPreviousPage = this.returnToPreviousPage.bind(this);
        this.handleModalDisplay = this.handleModalDisplay.bind(this);
        this.submitEditStackModalForm = this.submitEditStackModalForm.bind(this);
        this.setFolderName = this.setFolderName.bind(this);
        this.setFolderDescription = this.setFolderDescription.bind(this);
        this.fileInputHandler = this.fileInputHandler.bind(this);
        this.submitFileUpload = this.submitFileUpload.bind(this);
        this.showFileChooser = this.showFileChooser.bind(this);

        this.handleRadioSelection = this.handleRadioSelection.bind(this);
        this.renderAttachedFiles = this.renderAttachedFiles.bind(this);
        this.state = {
            showModal: false
        };

        this.selectionType = {
            FILE: 'file',
            SAVED_SEARCH: 'saved_search',
            STAK: 'stak'
        };
    }

    componentWillMount() {
        this.fileUploadSideInput = React.createRef();
        this.fileUploadFileInput = React.createRef();
        this.errorSpan = React.createRef();
        this.setState({
            attachedFiles: [],
            file: '',
            folderName: '',
            folderDescription: '',
            radioSelection: this.selectionType.FILE
        });
    }

    setFolderName(event) {
        this.setState({
            folderName: event.target.value
        });
    }

    setFolderDescription(event) {
        this.setState({
            folderDescription: event.target.value
        });
    }

    fileInputHandler(event) {
        event.preventDefault();
        const files = event.target.files;
        const file = files[0];
        this.setState({
            file: file
        });

        this.fileUploadSideInput.current.value = file.name;
    }

    submitFileUpload(folderId) {
        const file = this.state.file;

        if(file && folderId) {

            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => {
                const params = {
                    _USERID: "GLOBAL",
                    _FILEDATA: reader.result,
                    _FILENAME: file.name,
                    _FILEEXT: file.name.substring(file.name.indexOf('.') + 1, file.name.length),
                    _FILESIZE: file.size,
                    _CONTENTTYPE: file.type,
                    _SEARCHID: '',
                    SELECTFOLDERLISTID: folderId,
                    AUTHOR: '',
                    UPLOADEDBY: '',
                    TAGS: '',
                    DESCRIPTION: ''
                }

                const xmlhttp = new XMLHttpRequest();
                xmlhttp.open("POST", "http://10.10.1.202/api/Post/PostMetaFile/FormData", false);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

                /*xmlhttp.open("POST", "http://10.10.1.201/api/Upload/PostFile/FormData", false);

                xmlhttp.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImNjb2tlckBlbmNhcHNhLmNvbSIsIm5iZiI6MTUzMjI4NjI1MSwiZXhwIjoxNTMyODkxMDUxLCJpYXQiOjE1MzIyODYyNTEsImlzcyI6Imh0dHA6Ly8xMC4xMDEuMi4xMDE6ODAiLCJhdWQiOiJodHRwOi8vMTAuMTAxLjIuMTAxOjgwIn0.r5AaNueUxAnS9goMt7VteSagdQntS9aJF77dDixvu4s");
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");*/

                xmlhttp.onreadystatechange = function () {

                    if (xmlhttp.readyState == XMLHttpRequest.DONE) {

                        if (xmlhttp.status == 200) {
                            alert("Upload Successful.");
                        } else {
                            alert("Something went wrong. Upload Failed.");
                        }

                    }
                }

                xmlhttp.send(Object.keys(params).map(key => key + '=' + encodeURIComponent(params[key])).join('&'));
            }
        } else {
            alert("Something went wrong. Upload Failed. FILE OR STAK NOT AVAILABLE");
        }
    }

    findFolderId(searchResults, folderName) {
        console.log('this.findFolderId')
        console.log(searchResults);
        let appID = ''
        if (searchResults && searchResults.Row) {
            console.log(searchResults.Row);
            searchResults.Row.forEach(function (rowItem) {
                if(rowItem.FOLDERNAME === folderName) {
                    console.log(rowItem.FOLDERNAME + ' ' +folderName);
                    console.log(appID);
                    appID = rowItem.APP_FORMID;
                }
            });
        }
        return appID;
    }

    submitEditStackModalForm(event) {
        event.preventDefault();
        const {APP_FORMID, DATEADDED, FOLDERDESCRIPTION, FOLDERNAME, TIMEADDED, _TABLENAME} = this.props.tableItem;
        const folderName = this.state.folderName ? this.state.folderName : FOLDERNAME;
        const folderDescription = this.state.folderDescription ? this.state.folderDescription : FOLDERDESCRIPTION;
        const file = this.state.file;
        const findFolderId = this.findFolderId;
        const submitFileUpload = this.submitFileUpload;
        const returnToPreviousPage = this.returnToPreviousPage;
        console.log('DATA' + folderName + ' ' + folderDescription);

        this.errorSpan.current.classList.add("hidden");

        if (folderName !== '') {

            const params = {
                _TABLENAME: _TABLENAME,
                _USERID: "GLOBAL",
                APP_FORMID: APP_FORMID,
                DATEADDED: DATEADDED,
                TIMEADDED: TIMEADDED,
                FOLDERNAME: folderName,
                FOLDERDESCRIPTION: folderDescription
            }

            const xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", "http://10.10.1.202/api/enCapsaGlobalAPI/Submit/FormData", false);
            xmlhttp.setRequestHeader("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6ImNjb2tlckBlbmNhcHNhLmNvbSIsIm5iZiI6MTUzMjI4NjYwNywiZXhwIjoxNTMyODkxNDA3LCJpYXQiOjE1MzIyODY2MDcsImlzcyI6Imh0dHA6Ly8xMC4xMDEuMi4xMDE6ODAiLCJhdWQiOiJodHRwOi8vMTAuMTAxLjIuMTAxOjgwIn0.iXQ2yn87Motee79aroKKZMMwfh68-1DxCI9Hk65Jp5s");
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.onreadystatechange = function () {

                if (xmlhttp.readyState == XMLHttpRequest.DONE) {

                    if (xmlhttp.status == 200) {
                        console.log('FILE');
                        console.log(file);
                        if(file) {
                            const tableData = loadTableData('DOCUMENTFOLDER');
                            console.log('table data');
                            console.log(tableData);
                            tableData.then((response) => {
                                const searchResults = JSON.parse(response.data.replace(/\\\//g, "/"));
                                const appId = findFolderId(searchResults, folderName);
                                console.log(appId + ' ' + folderName);
                                console.log(searchResults);
                                submitFileUpload(appId);
                            }).catch( (error) => {
                                console.log('ERROR FETCHING FOLDERS');
                                console.log(error);
                                alert("FILE CREATION FAILED");
                            });
                        } else {
                            alert("Upload Successful.");
                            returnToPreviousPage();
                        }
                    } else {
                        alert("Something went wrong. Stak Creation Failed.");
                    }

                }
            }

            xmlhttp.send(Object.keys(params).map(key => key + '=' + encodeURIComponent(params[key])).join('&'));

        } else {
            this.errorSpan.current.classList.remove("hidden");
        }
    }

    showFileChooser() {
        event.preventDefault();
        this.fileUploadFileInput.current.click();
    }

    returnToPreviousPage() {
        this.props.setNavPage(this.props.navPageSource);
    }

    handleModalDisplay() {
        this.setState({
            showModal: !this.state.showModal
        })
    }

    handleRadioSelection(e) {
        this.setState({
            radioSelection: e.target.value
        });
    }

    renderAttachedFiles(attachedFiles) {
        console.log('attached files');
        console.log(attachedFiles);

    }

    render() {

        const { ATTACHED_FILES, FOLDERNAME } = this.props.tableItem;
        console.log('TABLE ITEM');
        console.log(this.props.tableItem);

        return (
            <div className='div-new-stak-container'>
                <div className='div-new-stak-header'>
                    <label>Edit Stak</label>
                </div>

                <div className="div-new-stak-field-02">
                    <div className="div-new-stak-row">
                        <div className="div-row-left-side-02 position-inline-class">
                            <label className="top-text-stak">New stak name</label>
                            <span ref={this.errorSpan} className="error hidden"> Stak name required</span>
                        </div>
                        <div className="div-row-right-side position-inline-class">
                            <input className="new-stak-input-text-01" type="text" placeholder={FOLDERNAME} onChange={this.setFolderName}/>
                        </div>
                    </div>

                    <div className="div-new-stak-row">
                        <div className="div-row-left-side-02 position-inline-class">
                            <label className="top-text top-text-stak">Description</label>
                        </div>
                        <div className="div-row-right-side position-inline-class">
                            <textarea rows="4" cols="50" placeholder="Stak Description" onChange={this.setFolderDescription}>
                            </textarea>
                        </div>
                    </div>

                    <div className="div-new-stak-row">
                        <div className="div-row-left-side position-inline-class">
                            <input type="radio" name="addStakType" onChange={this.handleRadioSelection} value={this.selectionType.FILE} defaultChecked/>
                            <label>Select File</label>
                        </div>
                        <div className="div-row-right-side position-inline-class">
                            <input ref={this.fileUploadFileInput} type="file" onChange={this.fileInputHandler} disabled={this.state.radioSelection != this.selectionType.FILE}/>
                            <input ref={this.fileUploadSideInput} type="text" className="new-stak-input-text-02" disabled/>
                            <button type="button" onClick={this.showFileChooser} className="with-text" disabled={this.state.radioSelection != this.selectionType.FILE}>Browse</button>
                        </div>
                    </div>

                    <div className="div-new-stak-row">
                        <div className="div-row-left-side position-inline-class">
                            <input type="radio" name="addStakType" onChange={this.handleRadioSelection} value={this.selectionType.SAVED_SEARCH}/>
                            <label>Select from Saved Search</label>
                        </div>
                        <div className="div-row-right-side position-inline-class">
                            <input type="file" disabled={this.state.radioSelection != this.selectionType.SAVED_SEARCH}/>
                            <select className="new-stak-input-select-02" disabled={this.state.radioSelection != this.selectionType.SAVED_SEARCH}>
                                <option>Stak 01</option>
                            </select>
                            <button type="button" onClick={this.showFileChooser} className="with-text" disabled={this.state.radioSelection != this.selectionType.SAVED_SEARCH}>Add to Stak</button>
                        </div>
                    </div>

                    <div className="div-new-stak-row">
                        <div className="div-row-left-side position-inline-class">
                            <input type="radio" name="addStakType" onChange={this.handleRadioSelection} value={this.selectionType.STAK}/>
                            <label>Search File to Stak</label>
                        </div>
                        <div className="div-row-right-side position-inline-class">
                            <input type="file" disabled={this.state.radioSelection != this.selectionType.STAK}/>
                            <input type="text" className="new-stak-input-text-02" disabled={this.state.radioSelection != this.selectionType.STAK}/>
                            <button type="button" onClick={this.handleModalDisplay} className="with-text" disabled={this.state.radioSelection != this.selectionType.STAK}>Search</button>
                        </div>
                    </div>

                </div>

                <SearchModal showModal={this.state.showModal} handleModalDisplay={this.handleModalDisplay} />

                <div className="div-new-stak-row">
                    <div className="div-edit-stak-files">
                        Files
                    </div>
                    <div>
                        <div className="div-row-right-side position-inline-class">
                            <select className="div-stak-dropdown">
                                <option selected="selected"> Click to View Attached Files</option>
                                {
                                    ATTACHED_FILES && ATTACHED_FILES.map((attachedFile) => {
                                        return (<option>{attachedFile._FILENAME} | {attachedFile._FILEEXT} | {attachedFile._FILESIZE}</option>);
                                    })
                                }
                            </select>
                        </div>

                    </div>
                </div>

                <div className="div-new-stak-field-02">
                    <div className="div-new-stak-row">
                        <button className="rounded-button-green-bg" onClick={this.submitEditStackModalForm}> Update </button>
                        <button className="rounded-button-white-bg rounded-button-spacing" onClick={this.returnToPreviousPage}> Cancel </button>
                    </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    console.log('EDIT STAK STATE');
    console.log(state);
    return {
        tableItem: state.navPage ? state.navPage.tableItem : '',
        navPageSource: state.navPage ? state.navPage.navPageSource : ''
    }
}


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setNavPage: setNavPage
    }, dispatch);
}

const EditStakModalContainer = connect(mapStateToProps, mapDispatchToProps)(EditStakModal);

export default EditStakModalContainer;