import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    hidePreviewStakModal
} from "../redux-store/actions/modal-actions";
import NoPreview from '../styles/assets/nopreview.jpg';
import Png from '../styles/assets/no-image-icon.png';
import Jpeg from '../styles/assets/no-image-icon.png';
import WordFile from '../styles/assets/no-image-icon.png';
import Txt from '../styles/assets/no-image-icon.png';
import RandomFile from '../styles/assets/no-image-icon.png';
import AdobeFile from '../styles/assets/no-image-icon.png';
import StakGreen from '../styles/assets/no-image-icon.png';

import FileViewer from 'react-file-viewer';

class PreviewStakModal extends Component {

    constructor(props) {
        super(props);
		this.displayContent = this.displayContent.bind(this);
		this.displayImage = this.displayImage.bind(this);
		this.displayStrategy = this.displayStrategy.bind(this);

		// TODO have to remove this after testing
        this.imageExtLookup = ['.png', '.jpg', '.jpeg', 'png', 'jpg', 'jpeg'];
    }

    componentWillMount() {

    }

    // TODO have to remove this after testing
    // NOTE: I think what they want is a document reader
    generateTemporaryContent(fileExt, folderName) {

        if(folderName) {
            return (<img src={StakGreen}/>);
        }

        switch (fileExt) {
            case '.pdf':
            case 'pdf':
                return (<img src={AdobeFile}/>);
            case '.txt':
            case 'txt':
                return (<img src={Txt}/>);
            case '.docx':
            case 'docx':
            case 'doc':
                return (<img src={WordFile}/>);
            case 'stak':
                return (<img src={StakGreen}/>);
            default:
                return (<img src={RandomFile}/>);
        }
    }

    // TODO have to remove this after testing
    displayContent(tableItem) {

    	if (!tableItem.FILEEXT || this.imageExtLookup.indexOf(tableItem.FILEEXT) !== -1) {
    		return null;
    	}

    	return this.generateTemporaryContent(tableItem.FILEEXT, tableItem.FOLDERNAME);
    }

    // TODO have to remove this after testing
    displayImage(tableItem) {

    	// NOTE: temp implementation until we figure out how to turn blob into a real img
    	// NOTE: initial research but need real data "https://stackoverflow.com/questions/7650587/using-javascript-to-display-blob"
    	return (<div>PLACE HOLDER</div>)
    }

    b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    displayStrategy() {
    	/*const imageExtLookup = this.imageExtLookup;
        const tableItem = this.props.tableItem;
        const displayContent = this.displayContent;
        const displayImage = this.displayImage;*/

        /*if (imageExtLookup.indexOf(tableItem.FILEEXT) === -1) {
    		return this.displayContent(tableItem)
    	} else {
    		return this.displayImage(tableItem)
    	}*/

		const tableItem = this.props.tableItem;

    	if (!tableItem.FILEDATA) {
    		return (<div>File Not Found</div>)
    	}

    	const urlCreator = window.URL || window.webkitURL;
    	const blob = this.b64toBlob(tableItem.FILEDATA, tableItem.CONTENTTYPE);

        let fileUrl = urlCreator.createObjectURL(blob);
    	let fileExt = '' + tableItem.FILEEXT;
        let getExtension = fileExt.lastIndexOf('.');
        
        if (getExtension != -1) {
            fileExt = fileExt.substring(getExtension);
            fileExt = fileExt.replace(/\./g,'');
        }
        
    	if (fileExt !== 'png' && fileExt !== 'jpeg' && fileExt !== 'gif' && 
            fileExt !== 'pdf' && fileExt !== 'csv' && fileExt !== 'xslx' && 
            fileExt !== 'docx') {
                fileUrl = '../styles/assets/nopreview';
                fileExt = 'jpg';
        }

    	return <FileViewer fileType={fileExt} filePath={fileUrl} />
    }

    render() {

    	/*const file = 'http://example.com/image.png'
		const type = 'png'*/
        
        const tableItem = this.props.tableItem;

        if (!tableItem || !tableItem.FILEEXT) {
        	return null;
        }

		if (this.props.displayModal) {
            return (
                <div className="div-modal-backdrop-container">
                    <div className="div-modal-container div-stak-modal">
                        <div className="div-modal-content">
                            <div>
                                <button onClick={this.props.hidePreviewStakModal} className='div-stak-close'> 
                                    <span class="span-preview-close">X</span>
                                </button>
                            </div>
                            <div className="div-modal-body div-stak-body">
                                <div className="div-new-stak-container div-stak-container">
                                    <div className="div-new-stak-row">
                                        {this.displayStrategy()}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }   	
    }
}

const mapStateToProps = (state) => {
    console.log('Preview Stak Modal State');
    console.log(state);
    return {
        tableItem: state.modal ? state.modal.tableItem : {}
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        hidePreviewStakModal
    }, dispatch);
}

const PreviewStakModalContainer = connect(mapStateToProps, mapDispatchToProps)(PreviewStakModal);

export default PreviewStakModalContainer;