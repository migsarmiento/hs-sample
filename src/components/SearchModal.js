import React, {Component} from 'react';
import {search} from './../services/service';
import Png from '../styles/assets/icons/png.png';
import Jpeg from '../styles/assets/icons/jpeg.png';
import WordFile from '../styles/assets/icons/word.png';
import Txt from '../styles/assets/icons/txt.png';
import RandomFile from '../styles/assets/icons/paper.png';
import AdobeFile from '../styles/assets/icons/adobe.png';
import Filters from './../components/Filters';

class SearchModal extends Component {

    constructor(props) {
        super(props);
        this.parseSearchResults = this.parseSearchResults.bind(this);
        this.setSearchInput = this.setSearchInput.bind(this);
        this.toggleSearch = this.toggleSearch.bind(this);
        this.renderSearchResults = this.renderSearchResults.bind(this);
        this.toggleSearchOnEnter = this.toggleSearchOnEnter.bind(this);
        this.addFilter = this.addFilter.bind(this);
        this.changeSelectedFilter = this.changeSelectedFilter.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.renderAdditionalFilter = this.renderAdditionalFilter.bind(this);
        this.renderBaseFilter = this.renderBaseFilter.bind(this);
        this.renderFilterField = this.renderFilterField.bind(this);
        this.renderNameFilter = this.renderNameFilter.bind(this);
        this.renderCreatedDateFilter = this.renderCreatedDateFilter.bind(this);
    }


    componentWillMount() {
        this.setState({
            searchInput: '',
            searchResults: [],
            filterRenderers: [],
            selectedFilters: [],
        });
    }

    componentDidMount() {
        this.addFilter();
    }

    addFilter() {
        console.log('add filter');
        this.state.filterRenderers.push(this.renderAdditionalFilter);
        this.setState({
            filterRenderers: this.state.filterRenderers,
            selectedFilters: this.state.selectedFilters
        })
    }

    changeSelectedFilter(selectedFilter, index) {
        this.state.filterRenderers[index] = this.renderBaseFilter;
        this.state.selectedFilters[index] = selectedFilter;
        this.setState({
            filterRenderers: this.state.filterRenderers,
            selectedFilters: this.state.selectedFilters
        });
        console.log(selectedFilter + ' ' + index);
    }

    renderAdditionalFilter(index) {
        console.log('RENDER ADDITIONAL FILTER' + index);
        let selectedFilter = '';
        if (index) {
            selectedFilter = this.state.selectedFilters[index];
        }
        if (selectedFilter) {
            console.log('change');
        } else {
            console.log('return render');
            return (
                <div className="filters-row-01">
                    <div className="filter-position-1 div-filename-select">
                        <select onChange={(event) => {
                            this.changeSelectedFilter(event.target.value, index);
                        }}>
                            <option className="defaultTextValue" value="" disabled selected hidden>File Name</option>
                            <option> File Name</option>
                            <option> File Type</option>
                            <option> Last opened date</option>
                            <option> Last modified date</option>
                            <option> Created date</option>
                        </select>


                    </div>
                    <div className="filter-position-1">
                        {this.renderNameFilter()}
                    </div>
                    <div className="filter-position-1 filter-footer">
                        <button className="circle-button-grey" onClick={ () => {this.removeFilter(index)}}>-</button>
                        <button className="circle-button-grey" onClick={this.addFilter}>+</button>
                    </div>
                    <hr/>
                </div>
            );
        }
    }

    renderBaseFilter(index) {
        return (
            <div className="filters-row-01">
                <div className="filter-position-1 div-filename-select">
                    <select className="home-folder-inline-position" onChange={(event) => {
                        this.changeSelectedFilter(event.target.value, index);
                    }}>
                        <option className="defaultTextValue" value="" disabled selected
                                hidden>{this.state.selectedFilters[index]}</option>
                        <option> File Name</option>
                        <option> File Type</option>
                        <option> Last opened date</option>
                        <option> Last modified date</option>
                        <option> Created date</option>
                    </select>
                </div>
                <div className="filter-position-1 div-filename-select">
                    {this.renderFilterField(this.state.selectedFilters[index])}
                </div>
                <div className="filter-footer">
                    <button className="circle-button-grey" onClick={ () => {this.removeFilter(index)}}>-</button>
                    <button className="circle-button-grey" onClick={this.addFilter}>+</button>
                </div>
                <hr/>
            </div>
        );
    }

    renderFilterField(selectedFilter) {
        console.log(this.state.selectedFilters);
        switch (selectedFilter) {
            case 'File Name': {
                return this.renderNameFilter();
            }
            case 'File Type': {
                return this.renderFileTypeFilters();
            }
            case 'Last opened date':
            case 'Last modified date':
            case 'Created date':
                return this.renderCreatedDateFilter();
        }
    }

    renderFileTypeFilters() {
        return (
            <div className="filter-position-1">
                <select>
                    <option className="defaultTextValue" value="" disable selected hidden></option>
                    <option>pdf</option>
                    <option>docx</option>
                    <option>xls</option>
                    <option>jpg/jpeg</option>
                    <option>png</option>
                    <option>txt</option>
                </select>
            </div>
        );
    }

    renderNameFilter() {
        return (
            <div className="filter-position-1">
                <input className="filter-input-field" type='text'></input>
            </div>
        );
    }

    renderCreatedDateFilter() {
        return (
            <div>
                <div className="filter-position-1 div-filename-select">
                    <select>
                        <option>Within Last</option>
                        <option>Exactly</option>
                        <option>Before</option>
                        <option>After</option>
                    </select>
                </div>
                <div className="filter-position-1 div-filename-select">
                    <select>
                        <option>Days</option>
                        <option>Weeks</option>
                        <option>Months</option>
                        <option>Years</option>
                    </select>
                </div>
                <div className="filter-position-1">
                    <input className="filter-input-field" type='text'></input>
                </div>

            </div>
        );
    }

    removeFilter(index) {
        console.log('remove filter' + index);
        if(index) {
            console.log('REMOVE THIS HSIT');
            console.log('remove filter' + index);
            this.state.filterRenderers.splice(index,1);
            this.state.selectedFilters.splice(index,1);
        }
        this.setState({
            filterRenderers: this.state.filterRenderers,
            selectedFilters: this.state.selectedFilters
        })
    }

    parseSearchResults(searchResults) {
        console.log('this.parseSearchResults')
        console.log(searchResults);
        let tableData = [];
        if (searchResults && searchResults.Row) {
            console.log(searchResults.Row);
            searchResults.Row.forEach(function (rowItem) {
                let tableItem = {};
                rowItem.SearchResultItems && rowItem.SearchResultItems.forEach(function (item) {
                    switch (item.FieldName) {
                        case 'FILENAME':
                        case '_FILENAME':
                        case 'FOLDERNAME':
                        case 'FILESIZE':
                        case '_FILESIZE':
                        case 'FILEEXT':
                        case '_FILEEXT':
                        case 'LASTMODIFIEDDATE':
                        case 'DATEADDED':
                            tableItem[item.FieldName] = item.Data;
                    }
                });
                console.log('Table Item');
                console.log(tableItem);
                tableData.push(tableItem);
            });
        }
        return tableData;
    }

    setSearchInput(event) {
        this.setState({
            textInput: event.target.value
        });
    }

    toggleSearch() {
        search(this.state.textInput).then((response) => {
            response.data.replace(/\\\//g, "/");
            this.setState({
                searchResults: JSON.parse(response.data)
            });
        }).catch((error) => {
            console.log('NO RESULTS');
            this.setState({
                searchResults: []
            });
        });
    }

    toggleSearchOnEnter(event) {
        if (event.key === 'Enter') {
            this.toggleSearch();
        }
    }

    generateIconByFileExt(fileExt) {
        console.log('generate icon');
        console.log(fileExt);
        switch (fileExt) {
            case 'png':
                return (<img src={Png}/>);
            case 'jpg':
            case 'jpeg':
                return (<img src={Jpeg}/>);
            case '.pdf':
            case 'pdf':
                return (<img src={AdobeFile}/>);
            case '.txt':
            case 'txt':
                return (<img src={Txt}/>);
            case '.docx':
            case 'docx':
            case 'doc':
                return (<img src={WordFile}/>);
            default:
                return (<img src={RandomFile}/>);
        }
    }

    formatBytes(a, b) {
        if (a) {
            const bytes = parseInt(a);
            if (0 == bytes) return "0 Bytes";
            var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                f = Math.floor(Math.log(bytes) / Math.log(c));
            return parseFloat((bytes / Math.pow(c, f)).toFixed(d)) + " " + e[f];
        } else {
            return '';
        }
    }

    renderSearchResults() {
        const tableData = this.parseSearchResults(this.state.searchResults);
        const resultCount = tableData.length;
        return (
            <div className='div-search-modal-container'>
                <div className='home-folder-content div-search-modal-headers'>
                    <div className='div-result-title'>
                        <label>{resultCount} results</label>
                    </div>
                    <div className='div-home-folder-headers'>
                        <div className="home-folder-inline-position div-name">
                            <label>Name</label>
                            <i className="arrow-down"></i>
                        </div>
                        <div className="home-folder-inline-position div-modified">
                            <label>Modified</label>
                            <i className="arrow-down"></i>
                        </div>
                        <div className="home-folder-inline-position div-date-added">
                            <label>Date Added</label>
                            <i className="arrow-down"></i>
                        </div>
                        <div className="home-folder-inline-position div-file-size">
                            <label>File Size</label>
                            <i className="arrow-down"></i>
                        </div>
                        <hr/>
                    </div>
                    {tableData && tableData.map(tableItem => {
                        return <div className="folder-detail-row search-results-row">
                            <div className="home-folder-inline-position search-results-inline-position div-name div">
                                <i className="file-icon">
                                    {this.generateIconByFileExt(tableItem.FILEEXT)}
                                </i>
                                <label>{tableItem.FILENAME || tableItem._FILENAME || tableItem.FOLDERNAME}</label>
                            </div>
                            <div className="home-folder-inline-position search-results-inline-position div-modified">
                                <label>{tableItem.LASTMODIFIEDDATE}</label>
                            </div>
                            <div className="home-folder-inline-position search-results-inline-position div-date-added">
                                <label>{tableItem.DATEADDED}</label>
                            </div>
                            <div className="home-folder-inline-position search-results-inline-position div-file-size">
                                <label>{this.formatBytes(tableItem.FILESIZE, 2)}</label>
                            </div>
                            <hr/>
                        </div>
                    })}
                </div>
            </div>
        );
    }

    render() {
        console.log(this.state.filterRenderers);
        if (this.props.showModal) {
            return (
                <div className="div-modal-backdrop-container">
                    <div className="div-modal-container">

                        <div className="div-modal-content">
                            <div className="div-modal-header">
                                <label>Search Stak Files</label>
                            </div>
                            <div className="div-modal-body">
                                <div className="div-new-stak-row">
                                    <input type='text' className='search-bar-modal'
                                           placeholder='Search your haystak drive' onChange={this.setSearchInput}
                                           onKeyPress={this.toggleSearchOnEnter}/>
                                    <button type='button' className='search-bar-modal-button'
                                            onClick={this.toggleSearch}>Search
                                    </button>
                                </div>
                                <hr/>
                                <div className="div-search-modal-filter">
                                {this.state.filterRenderers && this.state.filterRenderers.map((filterRenderer, index) => {
                                    return (<div className="div-new-stak-row">
                                        {filterRenderer(index)}
                                    </div>);
                                })}
                                </div>
                                <div className="div-new-stak-row">
                                    {this.renderSearchResults()}
                                </div>
                            </div>
                            <div className="div-modal-footer">
                                <button className="rounded-button-white-bg search-modal-button"
                                        onClick={this.props.handleModalDisplay}> CANCEL
                                </button>
                                <button className="rounded-button-green-bg search-modal-button"> ADD TO STAK</button>
                            </div>
                        </div>

                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}

export default SearchModal;