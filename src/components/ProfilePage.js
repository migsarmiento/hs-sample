import React, { Component } from 'react';
import {connect} from 'react-redux';
import {setNavPage} from './../redux-store/actions/nav-page-actions';
import {bindActionCreators} from 'redux';
import { Pie } from 'react-chartjs';

class ProfilePage extends Component {

    constructor(props) {
        super(props);
        this.navigateToLoginPage = this.navigateToLoginPage.bind(this);
        this.generateLegend = this.generateLegend.bind(this);
    }

    componentWillMount() {
        this.pieChartRef = React.createRef();
    }


    navigateToLoginPage() {
        this.props.setNavPage('Login');
    }

    generatePieChart() {
        const chartData = [
            {
                value: 500,
                color: "#FFFFFF",
                highlight: "#F1F1F1",
                label: "Free Space"
            },
            {
                value: 375,
                color: "#3C7040",
                highlight: "#3CA040",
                label: "File Type"
            },
            {
                value: 150,
                color:"#B1EDB6",
                highlight: "#B1FDB6",
                label: "File Type"
            },
            {
                value: 750,
                color:"#4FBC5C",
                highlight: "#4FDC5C",
                label: "File Type"
            }
        ];

        const options = {
            segmentStrokeColor: "#D8D8D8",
            segmentStrokeWidth: 1,
            legendTemplate: "<ul><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%> (<%=segments[i].value%> GB)<%}%></li><%}%></ul>"
        };

        const pieChart = (
        <div>
            <Pie data={chartData} options={options} ref={this.pieChartRef} width="250" height="250" />
        </div>
        );

        return pieChart;
    }

    generateLegend() {
        if(this.pieChartRef && this.pieChartRef.current) {
            const legend = this.pieChartRef.current.generateLegend();
            console.log(legend);
            document.getElementById('legend').innerHTML = legend;
        }
    }

    render() {
        return (
            <div className="container-fluid" onMouseMove={this.generateLegend}>
                <div className="row">
                    <div className="col-12">
                        <h3>My Profile</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-6">
                        <label>General Information</label>
                    </div>
                    <div className="col-6">
                        <a href="#">Settings</a>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-6">
                        <label>Name</label>
                    </div>
                    <div className="col-6">
                        <label>Ronald Bugundy</label>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-6">
                        <label>Email</label>
                    </div>
                    <div className="col-6">
                        <label>hello@ronburgundy.com</label>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-6">
                        <div id='disk-utilization'>
                            <label>Disk Utilization</label>
                        </div>
                        <div>
                            <label>123 GB used of 300 GB available</label>
                        </div>
                    </div>
                    <div className="col-4">
                        {this.generatePieChart()}
                    </div>
                    <div id='legend' className="col-2 chart-legend">
                    </div>
                </div>
                <hr />
            </div>
        );
    }

}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setNavPage: setNavPage
    }, dispatch);
}

const ProfilePageContainer = connect(null, mapDispatchToProps)(ProfilePage);

export default ProfilePageContainer;