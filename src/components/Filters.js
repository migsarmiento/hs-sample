import React, {Component} from 'react';
import moment from 'moment';
import {connect} from 'react-redux';
import {setSearchQuery, setSearchResults} from './../redux-store/actions/search-actions';
import {bindActionCreators} from 'redux';
import PlusButton from './../styles/assets/plus-button.png';
import MinusButton from './../styles/assets/minus-button.png';

class Filters extends Component {

    constructor(props) {
        super(props);

        this.addFilter = this.addFilter.bind(this);
        this.changeSelectedFilter = this.changeSelectedFilter.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.renderAdditionalFilter = this.renderAdditionalFilter.bind(this);
        this.renderSearchRowFilterButton = this.renderSearchRowFilterButton.bind(this);
        this.renderBaseFilter = this.renderBaseFilter.bind(this);
        this.renderFilterField = this.renderFilterField.bind(this);
        this.renderNameFilter = this.renderNameFilter.bind(this);
        this.renderCreatedDateFilter = this.renderCreatedDateFilter.bind(this);
    }

    componentWillMount() {
        this.state = {
            filterRenderers: [],
            selectedFilters: [],
        }
    }


    addFilter() {
        this.state.filterRenderers.push(this.renderAdditionalFilter);
        this.setState({
            filterRenderers: this.state.filterRenderers,
            selectedFilters: this.state.selectedFilters
        })
    }

    removeFilter() {
        console.log('remove filter');
        this.setState({
            filterRenderers: [],
            selectedFilters: []
        })
    }

    renderSearchRowFilterButton() {
        if (this.state.filterRenderers.length > 0) {
            return (
                <button id='remove-filter-button' onClick={this.removeFilter}>Filter</button>);
        } else {
            return (<button id='add-filter-button' onClick={this.addFilter}>Filter</button>);
        }
    }

    changeSelectedFilter(selectedFilter, index) {
        this.state.filterRenderers[index] = this.renderBaseFilter;
        this.state.selectedFilters[index] = selectedFilter;
        this.setState({
            filterRenderers: this.state.filterRenderers,
            selectedFilters: this.state.selectedFilters
        });
        console.log(selectedFilter + ' ' + index);
    }

    renderAdditionalFilter(index) {
        let selectedFilter = '';
        if (index) {
            selectedFilter = this.state.selectedFilters[index];
        }
        if (selectedFilter) {
            console.log('change');
        } else {
            return (
                <div className="filters-row-01">
                    <div className="filter-position-1 div-filename-select">
                        <select onChange={(event) => {
                            this.changeSelectedFilter(event.target.value, index);
                        }}>
                            <option className="defaultTextValue" value="" disabled selected hidden>File Name</option>
                            <option> File Name</option>
                            <option> File Type</option>
                            <option> Last opened date</option>
                            <option> Last modified date</option>
                            <option> Created date</option>
                        </select>
                       
                        
                    </div>
                     <div className="filter-position-1">
                            {this.renderNameFilter()}
                        </div>
                    <button className="circle-button-grey" onClick={this.addFilter}>+</button>
                    <hr/>

                </div>
            );
        }
    }

    renderBaseFilter(index) {
        return (
            <div className="filters-row-01">
            <div className="filter-position-1 div-filename-select">
                <select className="home-folder-inline-position" onChange={(event) => {
                    this.changeSelectedFilter(event.target.value, index);
                }}>
                    <option className="defaultTextValue" value="" disabled selected
                            hidden>{this.state.selectedFilters[index]}</option>
                    <option> File Name</option>
                    <option> File Type</option>
                    <option> Last opened date</option>
                    <option> Last modified date</option>
                    <option> Created date</option>
                </select>
            </div>
            <div className="filter-position-1 div-filename-select">
            {this.renderFilterField(this.state.selectedFilters[index])}
            </div>
            <button onClick={this.addFilter}><img src={PlusButton}></img></button>
            <hr/>

        </div>
        );
    }

    renderFilterField(selectedFilter) {
        console.log(this.state.selectedFilters);
        switch (selectedFilter) {
            case 'File Name': {
                return this.renderNameFilter();
            }
            case 'File Type': {
                return this.renderFileTypeFilters();
            }
            case 'Last opened date':
            case 'Last modified date':
            case 'Created date':
                return this.renderCreatedDateFilter();
        }
    }

    renderFileTypeFilters() {
        return (
            <div className="filter-position-1">
                <select>
                    <option className="defaultTextValue" value="" disable selected hidden></option>
                    <option>pdf</option>
                    <option>docx</option>
                    <option>xls</option>
                    <option>jpg/jpeg</option>
                    <option>png</option>
                    <option>txt</option>
                </select>
            </div>
        );
    }

    renderNameFilter() {
        return (
            <div className="filter-position-1">
                <input className="filter-input-field" type='text'></input>
            </div>
        );
    }

    renderCreatedDateFilter() {
        return (
            <div>
                <div className="filter-position-1 div-filename-select">
                    <select>
                        <option>Within Last</option>
                        <option>Exactly</option>
                        <option>Before</option>
                        <option>After</option>
                    </select>
                </div>
                <div className="filter-position-1 div-filename-select">
                    <select>
                        <option>Days</option>
                        <option>Weeks</option>
                        <option>Months</option>
                        <option>Years</option>
                    </select>
                </div>
                <div className="filter-position-1">
                    <input className="filter-input-field" type='text'></input>
                </div>
               
            </div>
        );
    }

    render() {
        console.log(this.state.filterRenderers);
        return (
            <div className='div-filters'>
                <div className='search-result-field'>
                    <label>Search: " <label className="search-result">{this.props.searchQuery}</label> "</label>
                    {this.renderSearchRowFilterButton()}
                </div>
                <hr/>

                {this.state.filterRenderers && this.state.filterRenderers.map((filterRenderer, index) => {
                    return (filterRenderer(index));
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        searchQuery: state.search ? state.search.searchQuery : ''
    };
}

const FiltersContainer = connect(mapStateToProps, null)(Filters);

export default FiltersContainer;