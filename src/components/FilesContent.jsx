import React, {Component} from 'react';
import moment from 'moment';
import noImageIcon from './../styles/assets/no-image-icon.png';
import {connect} from 'react-redux';
import {setSearchQuery, setSearchResults} from './../redux-store/actions/search-actions';
import {bindActionCreators} from 'redux';

// needs proper files
import Png from '../styles/assets/no-image-icon.png';
import Jpeg from '../styles/assets/no-image-icon.png';
import WordFile from '../styles/assets/no-image-icon.png';
import Txt from '../styles/assets/no-image-icon.png';
import RandomFile from '../styles/assets/no-image-icon.png';
import AdobeFile from '../styles/assets/no-image-icon.png';
import StakGreen from '../styles/assets/no-image-icon.png';

class FilesContent extends Component {

    constructor(props) {
        super(props);

        this.showContextMenu = this.showContextMenu.bind(this);

    }

    generateIconByFileExt(fileExt) {
        switch (fileExt) {
            case 'png':
                return (<img src={Png}/>);
            case '.jpg':
            case '.jpeg':
                return (<img src={Jpeg}/>);
            case '.pdf':
            case 'pdf':
                return (<img src={AdobeFile}/>);
            case '.txt':
            case 'txt':
                return (<img src={Txt}/>);
            case '.docx':
            case 'docx':
            case 'doc':
                return (<img src={WordFile}/>);
            case 'stak':
                return (<img src={StakGreen}/>);
            default:
                return (<img src={RandomFile}/>);
        }
    }

    componentWillMount() {
        this.setState({
            contextMenuShown: false
        });
    }

    showContextMenu(e) {
        e.preventDefault();

        // console.log(e.target.parentElement.getAttribute("data-id"));

        if (e.type == 'contextmenu') {
            this.setState({contextMenuShown: true});
        }

    }

    render() {

        const generateIconByFileExt = this.generateIconByFileExt;

        return (
            <div className='div-file-content'>
                {this.props.data && this.props.data.Row.map(fileData => {
                    return (
                        <div className="file-box">
                            <div className="file-img-box-container">
                                <div className="file-img-box" onContextMenu={this.showContextMenu} data-id={fileData.FILEID}>
                                    {generateIconByFileExt(fileData.FILEEXT)}
                                </div>
                            </div>
                            <label> {fileData.FILENAME} </label>
                        </div>
                    );
                })}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    console.log('STATES');
    console.log(state);
    return {
        searchQuery: state.search ? state.search.searchQuery : '',
        searchResults: state.search ? state.search.searchResults : []
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({}, dispatch);
}

const FilesContentContainer = connect(mapStateToProps, mapDispatchToProps)(FilesContent);

export default FilesContentContainer;