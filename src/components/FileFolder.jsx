import React, {Component} from 'react';
import Upload from './../components/Upload';
import {connect} from 'react-redux';
import {saveData} from './../services/service';
import {bindActionCreators} from 'redux';
import {
    showNewStakModal,
    showConnectModal,
    hideNewStakModal,
    hideConnectModal
} from "../redux-store/actions/modal-actions";
import DownArrow from './../styles/assets/DownArrow.png';
import ListView from './../styles/assets/ListView.png';
import GridView from './../styles/assets/GridView.png';
import Folder from './../styles/assets/Folder.png';
import StakGray from './../styles/assets/Stak Gray.png';
import Png from '../styles/assets/icons/png.png';
import Jpeg from '../styles/assets/icons/jpeg.png';
import WordFile from '../styles/assets/icons/word.png';
import Txt from '../styles/assets/icons/txt.png';
import RandomFile from '../styles/assets/icons/paper.png';
import AdobeFile from '../styles/assets/icons/adobe.png';
import StakGreen from '../styles/assets/Stak Green.png';
import {setNavPage} from "../redux-store/actions/nav-page-actions";

import FileDisplayLayout from '../components/FileDisplayLayout';

class FileFolder extends Component {

    constructor(props) {
        super(props);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.uploadFiles = this.uploadFiles.bind(this);
        this.openFileUploadMenu = this.openFileUploadMenu.bind(this);
        this.showNewStakPage = this.showNewStakPage.bind(this);
        this.parseSearchResultsWithRow = this.parseSearchResultsWithRow.bind(this);
        this.search = this.search.bind(this);
    }

    componentWillMount() {
        this.fileUpload = React.createRef();
        this.formSubmit = React.createRef();
        this.setState({
            file: '',
            param: {},
            searchResults: ''
        })

        this.search('(?:dr.|robert|bob)(?: s.)? (?:kite)(?:\\, ph.d.)?');
    }

    componentWillReceiveProps() {
        console.log('change fiel folder');
        this.search('(?:dr.|robert|bob)(?: s.)? (?:kite)(?:\\, ph.d.)?');
    }

    search(searchQuery) {
        const params = {
            _SEARCHID: searchQuery
        };
        const setSearchResults = this.props.setSearchResults;

        const xmlHttp = new XMLHttpRequest();
        xmlHttp.open('POST', 'http://10.10.1.202/api/Search/PostSearch/FormData', false);
        xmlHttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xmlHttp.onreadystatechange = () => {
            if (xmlHttp.readyState == XMLHttpRequest.DONE) {
                if (xmlHttp.status == 200) {
                    const response = xmlHttp.responseText;
                    console.log('SUCCESS');
                    console.log(response);
                    if (response && response !== '"NOT FOUND"') {
                        console.log('set state');
                        this.setState({
                            searchResults: JSON.parse(response.replace(/\\\//g, "/"))
                        });
                    } else {
                        this.setState({
                            searchResults: ''
                        });
                    }
                } else {
                    this.setState({
                        searchResults: ''
                    });
                }
            }

        }
        xmlHttp.send(Object.keys(params).map(key => key + '=' + encodeURIComponent(params[key])).join('&'));
    }

    parseSearchResultsWithRow(searchResultsString) {
        console.log('this.parseSearchResults Row')
        console.log(searchResultsString);
        const searchResults = JSON.parse(searchResultsString);
        console.log(searchResults);
        console.log(searchResults.Row);
        let tableData = {
            Row: []
        };
        if (searchResults && searchResults.Row) {
            console.log(searchResults.Row);
            searchResults.Row.forEach(function (rowItem) {
                let tableItem = {};
                rowItem.SearchResultItems && rowItem.SearchResultItems.forEach(function (item) {
                    switch (item.FieldName) {
                        case 'FILEID':
                        case 'FULLFILENAME':
                        case 'CONTENTTYPE':
                        case 'FILENAME':
                        case '_FILENAME':
                        case 'FOLDERNAME':
                        case 'FILESIZE':
                        case '_FILESIZE':
                        case 'FILEEXT':
                        case '_FILEEXT':
                        case 'LASTMODIFIEDDATE':
                        case 'DATEADDED':
                            tableItem[item.FieldName] = item.Data;
                    }
                });
                console.log('Table Item FILE FOLDER');
                console.log(tableItem);
                if(!tableItem.FOLDERNAME) {
                    tableData.Row.push(tableItem);
                }
            });
        }
        return tableData;
    }

    onSubmit(event) {
        event.preventDefault();
        console.log('Test');
    }

    handleFileUpload(event) {
        event.preventDefault();

        const files = event.target.files;
        const file = files[0];
        const reader = new FileReader();

        reader.readAsDataURL(file);

        reader.onload = () => {
            const params = {
                _TABLENAME: "FILEUPLOAD",
                _FILEDATA: reader.result,
                _FILENAME: file.name,
                _FILEEXT: file.name.substring(file.name.indexOf('.') + 1, file.name.length),
                _FILESIZE: file.size,
                _CONTENTTYPE: file.type
            }

            var xmlhttp = new XMLHttpRequest();

            xmlhttp.open("POST", "http://api.encapsa.com/api/Post/PostFile/FormData", false);

            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xmlhttp.send(Object.keys(params).map(key => key + '=' + params[key]).join('&'));

//            this.setState({
//                param: Object.keys(params).map(key => key + '=' + params[key]).join('&')
//            });

            this.formSubmit.current.click();
        }
    }

    uploadFiles() {
        console.log('upload files')
        /*if(this.state.file) {
            saveData(this.state.file).then(function(response) {
                console.log("Successfully Uploaded Files");
            }).catch(function(error) {
                console.log("Error uploading Files");
            });
        }*/
        if (this.state.param) {
            /*saveData(this.state.param).then(function(response) {
                console.log("Successfully Uploaded Files");
            }).catch(function(error) {
                console.log(error);
                console.log("Error uploading Files");
            });*/
        }
    }

    openFileUploadMenu(event) {
        event.preventDefault();
        console.log(this.fileUpload.current);
        this.fileUpload.current.click();
    }

    formatBytes(a, b) {
        if (a) {
            const bytes = parseInt(a);
            if (0 == bytes) return "0 Bytes";
            var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                f = Math.floor(Math.log(bytes) / Math.log(c));
            return parseFloat((bytes / Math.pow(c, f)).toFixed(d)) + " " + e[f];
        } else {
            return '';
        }
    }

    generateIconByFileExt(fileExt) {
        switch (fileExt) {
            case 'png':
                return (<img src={Png}/>);
            case 'jpg':
            case 'jpeg':
                return (<img src={Jpeg}/>);
            case '.pdf':
            case 'pdf':
                return (<img src={AdobeFile}/>);
            case '.txt':
            case 'txt':
                return (<img src={Txt}/>);
            case '.docx':
            case 'docx':
            case 'doc':
                return (<img src={WordFile}/>);
            case 'stak':
                return (<img src={StakGreen}/>);
            default:
                return (<img src={RandomFile}/>);
        }
    }

    showNewStakPage(event) {
        event.preventDefault();
        this.props.setNavPage('NewStak', 'Files');
    }

    render() {
        let tableData = '';
        if(this.state.searchResults) {
            tableData = this.parseSearchResultsWithRow(this.state.searchResults);
        }

        const data = {
            header: {
                iconClass: {
                    list: 'pull-right-88',
                    grid: ''
                },
                list: [
                    {label: 'Name', className: 'home-folder-inline-position div-name home-folder-header'},
                    {label: 'Modified', className: 'home-folder-inline-position div-modified home-folder-header'},
                    {label: 'Date Added', className: 'home-folder-inline-position div-date-added home-folder-header'},
                    {label: 'File Size', className: 'home-folder-inline-position div-file-size home-folder-header'}
                ],
                grid: [{label: 'Name', className: 'home-folder-inline-position div-name'}]
            },
            fileData: tableData,
            options: {
                defaultLayout: 'list',
                enableContextMenu: true
            }
        };

        return (
            <div className='div-home-folder-container'>
                <div className='home-folder-inline-position div-home-folder-content-01'>
                    <div className='div-home-folder-title'>
                        <label className="headers-font">Files</label>
                    </div>

                    <FileDisplayLayout data={data}/>

                </div>
                <div className="home-folder-inline-position div-home-folder-content-02">

                    <div>
                        <button id='save-button' class='save-btn' onClick={this.props.showConnectModal}>Connect</button>
                    </div>
                    <div className="div-new-stak" onClick={this.showNewStakPage}>
                        <i className="folder">
                            <img src={StakGray}/>
                        </i>
                        <label className="headers-font">New Stak</label>
                    </div>

                </div>

            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {showConnectModalState: state.modal ? state.modal.showConnectModal : false};
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            showNewStakModal: showNewStakModal,
            showConnectModal: showConnectModal,
            hideNewStakModal: hideNewStakModal,
            hideConnectModal: hideConnectModal,
            setNavPage: setNavPage
        }, dispatch);
}

const FileFolderContainer = connect(mapStateToProps, mapDispatchToProps)(FileFolder);

export default FileFolderContainer;