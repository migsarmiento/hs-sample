import React, {Component} from 'react';
import Upload from './../components/Upload';
import {connect} from 'react-redux';
import {saveData, loadTableData} from './../services/service';
import {bindActionCreators} from 'redux';
import {
    showNewStakModal,
    showConnectModal,
    hideNewStakModal,
    hideConnectModal, showEditStakModal, hideEditStakModal
} from "../redux-store/actions/modal-actions";
import DownArrow from './../styles/assets/DownArrow.png';
import ListView from './../styles/assets/ListView.png';
import GridView from './../styles/assets/GridView.png';
import Folder from './../styles/assets/Folder.png';
import Png from '../styles/assets/icons/png.png';
import Jpeg from '../styles/assets/icons/jpeg.png';
import WordFile from '../styles/assets/icons/word.png';
import Txt from '../styles/assets/icons/txt.png';
import RandomFile from '../styles/assets/icons/paper.png';
import AdobeFile from '../styles/assets/icons/adobe.png';
import StakGreen from '../styles/assets/Stak Green.png';
import {setNavPage} from "../redux-store/actions/nav-page-actions";
import FileDisplayLayout from '../components/FileDisplayLayout';
import StakGray from './../styles/assets/Stak Gray.png';

class StakFolder extends Component {

    constructor(props) {
        super(props);
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.uploadFiles = this.uploadFiles.bind(this);
        this.openFileUploadMenu = this.openFileUploadMenu.bind(this);
        this.showNewStakPage = this.showNewStakPage.bind(this);
        this.retrieveStaks = this.retrieveStaks.bind(this);
        this.showEditStakPage = this.showEditStakPage.bind(this);
    }

    displayTableData() {
        const mockData = {
            "Row": [
                {
                    "FILEID": "335232859536",
                    "FILENAME": "Windows Server Drive",
                    "FILESIZE": "65437",
                    "FILEEXT": "stak",
                    "LASTMODIFIEDDATE": "7/15/2018",
                    "LASTMODIFIEDTIME": "4:25PM",
                    "CONTENTTYPE": "application/pdf",
                    "CONTAINERID": "53A4BA7AB971273",
                    "OWNERID": "386c81ae-26ee-4999-9289-703cd994d0ab",
                    "OWNERNAME": "Christopher Coker",
                    "DATEADDED": "7/15/2018",
                    "TIMEADDED": "4:25PM"
                },
                {
                    "FILEID": "366177329177",
                    "FILENAME": "White Hat Drive",
                    "FILESIZE": "11494",
                    "FILEEXT": "stak",
                    "LASTMODIFIEDDATE": "7/15/2018",
                    "LASTMODIFIEDTIME": "3:48AM",
                    "CONTENTTYPE": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    "CONTAINERID": "u0026nbsp;",
                    "OWNERID": "u0026nbsp;",
                    "OWNERNAME": "u0026nbsp;",
                    "DATEADDED": "7/15/2018",
                    "TIMEADDED": "3:48AM"
                },
                {
                    "FILEID": "482414594779",
                    "FILENAME": "Red Hat Drive",
                    "FILESIZE": "32",
                    "FILEEXT": "stak",
                    "LASTMODIFIEDDATE": "7/15/2018",
                    "LASTMODIFIEDTIME": "3:48AM",
                    "CONTENTTYPE": "text/plain",
                    "CONTAINERID": "u0026nbsp;",
                    "OWNERID": "u0026nbsp;",
                    "OWNERNAME": "u0026nbsp;",
                    "DATEADDED": "7/15/2018",
                    "TIMEADDED": "3:48AM"
                },
                {
                    "FILEID": "926965372956",
                    "FILENAME": "CENT OS Drive",
                    "FILESIZE": "8587",
                    "FILEEXT": "stak",
                    "LASTMODIFIEDDATE": "7/15/2018",
                    "LASTMODIFIEDTIME": "3:48AM",
                    "CONTENTTYPE": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                    "CONTAINERID": "u0026nbsp;",
                    "OWNERID": "u0026nbsp;",
                    "OWNERNAME": "u0026nbsp;",
                    "DATEADDED": "7/15/2018",
                    "TIMEADDED": "3:48AM"
                }
            ]
        };

        return mockData;
    }

    componentWillMount() {
        this.fileUpload = React.createRef();
        this.formSubmit = React.createRef();
        this.setState({
            file: '',
            param: {},
            tableData: {},
        });

        this.retrieveStaks();
    }

    onSubmit(event) {
        event.preventDefault();
        console.log('Test');
    }

    handleFileUpload(event) {
        event.preventDefault();

        const files = event.target.files;
        const file = files[0];
        const reader = new FileReader();

        reader.readAsDataURL(file);

        reader.onload = () => {
            const params = {
                _TABLENAME: "FILEUPLOAD",
                _FILEDATA: reader.result,
                _FILENAME: file.name,
                _FILEEXT: file.name.substring(file.name.indexOf('.') + 1, file.name.length),
                _FILESIZE: file.size,
                _CONTENTTYPE: file.type
            }

            var xmlhttp = new XMLHttpRequest();

            xmlhttp.open("POST", "http://api.encapsa.com/api/Post/PostFile/FormData", false);

            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xmlhttp.send(Object.keys(params).map(key => key + '=' + params[key]).join('&'));

//            this.setState({
//                param: Object.keys(params).map(key => key + '=' + params[key]).join('&')
//            });

            this.formSubmit.current.click();
        }
    }

    uploadFiles() {
        console.log('upload files')
        /*if(this.state.file) {
            saveData(this.state.file).then(function(response) {
                console.log("Successfully Uploaded Files");
            }).catch(function(error) {
                console.log("Error uploading Files");
            });
        }*/
        if (this.state.param) {
            /*saveData(this.state.param).then(function(response) {
                console.log("Successfully Uploaded Files");
            }).catch(function(error) {
                console.log(error);
                console.log("Error uploading Files");
            });*/
        }
    }

    openFileUploadMenu(event) {
        event.preventDefault();
        console.log(this.fileUpload.current);
        this.fileUpload.current.click();
    }

    formatBytes(a, b) {
        if (a) {
            const bytes = parseInt(a);
            if (0 == bytes) return "0 Bytes";
            var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                f = Math.floor(Math.log(bytes) / Math.log(c));
            return parseFloat((bytes / Math.pow(c, f)).toFixed(d)) + " " + e[f];
        } else {
            return '';
        }
    }

    showNewStakPage(event) {
        event.preventDefault();
        this.props.setNavPage('NewStak', 'Stak');
    }

    retrieveStaks() {
        loadTableData('DOCUMENTFOLDER').then((response) => {
            let tableData = response.data;
            tableData = JSON.parse(tableData);
            console.log('document folder');
            console.log(tableData);
            this.setState({
                tableData: tableData
            });
        }).catch( (error) => {
            console.log(error);
        });
    }

    showEditStakPage(tableItem) {
        console.log('table item');
        console.log(tableItem);
        if(tableItem && tableItem.APP_FORMID) {
            const promise = loadTableData(tableItem.APP_FORMID);
            promise.then((response) => {
                const attachedFiles = JSON.parse(response.data.replace(/\\\//g, "/"));
                console.log('ATTACHED FILES');
                console.log(attachedFiles.Row);
                const files = attachedFiles.Row;

                if(files && files.length > 0) {
                    tableItem.ATTACHED_FILES = files;
                }

                this.props.setNavPage('EditStak', 'Stak', tableItem);
            });
        }
        console.log('Show Edit Stak');
        console.log(tableItem);

    }

    render() {

        if (!Object.keys(this.state.tableData).length) {
            return (
                <div className='div-home-folder-container'>
                    <div className='home-folder-inline-position div-home-folder-content-01'>
                        <div className='div-home-folder-title'>
                            <label>Staks</label>
                        </div>
                    </div>
                    <div className="home-folder-inline-position div-home-folder-content-02">

                        <div>
                            <button id='save-button' class='save-btn' onClick={this.props.showConnectModal}>Connect</button>
                        </div>
                        <div className="div-new-stak" onClick={this.showNewStakPage}>
                            <i className="folder">
                                <img src={Folder}/>
                            </i>
                            <label>New Stak</label>
                        </div>

                    </div>

                </div>
            );
        }

        const data = {
            header: {
                iconClass: {
                    list: 'pull-right-88',
                    grid: ''
                },
                list: [
                    {label: 'Name', className: 'home-folder-inline-position div-name'},
                    {label: 'Modified', className: 'home-folder-inline-position div-modified'},
                    {label: 'Date Added', className: 'home-folder-inline-position div-date-added'},
                    {label: 'File Size', className: 'home-folder-inline-position div-file-size'}
                ],
                grid: [{label: 'Name', className: 'home-folder-inline-position div-name'}]
            },
            fileData: this.state.tableData,
            options: {
                defaultLayout: 'list'
            }
        };

        return (
            <div className='div-home-folder-container'>
                <div className='home-folder-inline-position div-home-folder-content-01'>
                    <div className='div-home-folder-title'>
                        <label>Staks</label>
                    </div>

                    <FileDisplayLayout data={data} onFileContentClick={this.showEditStakPage}/>
                </div>
                <div className="home-folder-inline-position div-home-folder-content-02">

                    <div>
                        <button id='save-button' class='save-btn' onClick={this.props.showConnectModal}>Connect</button>
                    </div>
                    <div className="div-new-stak" onClick={this.showNewStakPage}>
                        <i className="folder">
                            <img src={StakGray}/>
                        </i>
                        <label>New Stak</label>
                    </div>

                </div>

            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators(
        {
            showNewStakModal: showNewStakModal,
            showConnectModal: showConnectModal,
            hideNewStakModal: hideNewStakModal,
            hideConnectModal: hideConnectModal,
            setNavPage: setNavPage,
        }, dispatch);
}

const StakFolderContainer = connect(null, mapDispatchToProps)(StakFolder);

export default StakFolderContainer;