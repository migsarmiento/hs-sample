import React, {Component} from 'react';
import './../styles/Login.css';
import GoogleIcon from './../styles/assets/google.png'
import BackgroundImage from './../styles/assets/HS-DG-UI-V2 (background).png'
import LogoHeader from "./LogoHeader";
import {connect} from 'react-redux';
import {setNavPage} from './../redux-store/actions/nav-page-actions';
import {bindActionCreators} from 'redux';

class Login extends Component {

    constructor(props) {
        super(props);
        this.navigateToHomePage = this.navigateToHomePage.bind(this);
    }

    navigateToHomePage(event) {
        event.preventDefault();
        this.props.setNavPage('Home');
    }

    render() {
        return (
            <div className='container'>
                <img className="login-background-image" src={BackgroundImage}/>
                <LogoHeader/>
                <div className="row">

                    <div className="col-6">

                        <div className="row">
                            <div className='col-6'><label className="sign-in">sign in</label></div>

                            <div className='col-6 account-create'>
                                <label>or  </label>
                                <label className='workaround-label'>_</label>
                                <a href="#">Create an Account.</a>
                            </div>
                        </div>

                        <div className='row'>
                            <button className="rounded-signin-google">
                                <img src={GoogleIcon} className="invert pull-left"/>
                                <label>SIGN IN WITH GOOGLE</label>
                            </button>
                        </div>

                        <hr className="sign-in-line"/>

                        <form method="post" action="#">

                            <div className="row">
                                <input type="text" placeholder="Email Address" name="email" className="rounded-input input-email"/>
                            </div>

                            <div className="row">
                                <input type="password" placeholder="Password" name="password"
                                       className="rounded-input input-password"/>
                            </div>

                            <div className="row forgot-password">
                                <a href="#">Forgot Password?</a>
                            </div>

                            <div className="row">

                                <div className="col-6 remember-me-row">
                                    <input id="remember-me" type="checkbox" name="remember-me"/><label
                                    for="remember-me"> Remember me</label>
                                </div>

                                <div className="col-6">
                                    <button type="submit" class="rounded-signin" onClick={this.navigateToHomePage}>SIGN
                                        IN
                                    </button>
                                </div>

                            </div>


                        </form>

                        <div className="row"></div>
                        <div className="row"></div>
                        <div className="row">
                            <ul className="footer copyright-footer">
                                <li>copyright &copy; 2018 haystak.app</li>
                                <li>all rights reserved</li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>

        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        setNavPage: setNavPage
    }, dispatch);
}

const LoginContainer = connect(null, mapDispatchToProps)(Login);

export default LoginContainer;
