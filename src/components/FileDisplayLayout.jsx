import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import DownArrow from './../styles/assets/DownArrow.png';
import ListView from './../styles/assets/ListView.png';
import GridView from './../styles/assets/GridView.png';
import Folder from './../styles/assets/Folder.png';
import {downloadFile} from './../services/service';
import {showNewStakModal, showPreviewStakModal} from "../redux-store/actions/modal-actions";

// this is for grid
import Png from '../styles/assets/no-image-icon.png';
import Jpeg from '../styles/assets/no-image-icon.png';
import WordFile from '../styles/assets/no-image-icon.png';
import Txt from '../styles/assets/no-image-icon.png';
import RandomFile from '../styles/assets/no-image-icon.png';
import AdobeFile from '../styles/assets/no-image-icon.png';
import StakGreen from '../styles/assets/no-image-icon.png';

// for list
import PngList from '../styles/assets/icons/paper.png';
import JpegList from '../styles/assets/icons/jpeg.png';
import WordFileList from '../styles/assets/icons/word.png';
import TxtList from '../styles/assets/icons/txt.png';
import RandomFileList from '../styles/assets/icons/paper.png';
import AdobeFileList from '../styles/assets/icons/adobe.png';
import StakGreenList from '../styles/assets/Stak Green.png';

class FileDisplayLayout extends Component {

    constructor(props) {
        super(props);

        this.displayedLayout = {
            GRID: 'grid',
            LIST: 'list'
        };

        this.displayLayout = this.displayLayout.bind(this);
        this.displayListLayout = this.displayListLayout.bind(this);
        this.displayGridLayout = this.displayGridLayout.bind(this);

        this.displayHeaderControls = this.displayHeaderControls.bind(this);
        this.clickPreview = this.clickPreview.bind(this);
    }

    componentWillMount() {

        this.setState({
            displayedLayout: this.props.data.options.defaultLayout
        });

    }

    displayListLayout() {
        this.setState({displayedLayout: this.displayedLayout.LIST});
    }

    displayGridLayout() {
        this.setState({displayedLayout: this.displayedLayout.GRID});
    }

    formatBytes(a, b) {
        if (a) {
            const bytes = parseInt(a);
            if (0 == bytes) return "0 Bytes";
            var c = 1024, d = b || 2, e = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"],
                f = Math.floor(Math.log(bytes) / Math.log(c));
            return parseFloat((bytes / Math.pow(c, f)).toFixed(d)) + " " + e[f];
        } else {
            return '';
        }
    }

    b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);

            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    clickDownloadFile(tableItem) {
        console.log("TABLE ITEM");
        console.log(tableItem);
        if(tableItem && tableItem.FILEID) {
            const promise = downloadFile(tableItem.FILEID);
            promise.then((response) => {
                console.log(response);
                const data = JSON.parse(response.data);
                const FILEDATA = data.Row[0].FILEDATA;
                const CONTENTTYPE = data.Row[0].CONTENTTYPE;
                //const blob = b64toBlob(FILEDATA, CONTENTTYPE)
                console.log('FILE DATA');
                console.log(data.Row[0].FILEDATA);
                const content = `data:application/octet-stream;charset=utf-8;base64,${FILEDATA}`;
                console.log('content');
                console.log(content);
                window.open(content, '_blank');
            });

        }
    }

    clickPreview(tableItem) {

        if(tableItem && tableItem.FILEID && (tableItem.FILEEXT === 'pdf' || tableItem.FILEEXT === 'txt')) {
            const promise = downloadFile(tableItem.FILEID);
            promise.then( (response) => {
                const data = JSON.parse(response.data);
                const fileData = data.Row[0];
                const base64 = fileData.FILEDATA;
                const blob = this.b64toBlob(base64, fileData.CONTENTTYPE);
                const urlCreator = window.URL || window.webkitURL;
                const fileUrl = urlCreator.createObjectURL(blob);
                console.log('file url : ' + fileUrl);
                window.open(fileUrl);
            });
            return;
        } else if(tableItem && tableItem.FILEID) {
            const promise = downloadFile(tableItem.FILEID);
            const showPreviewStakModal = this.props.showPreviewStakModal;
            promise.then((response) => {
                console.log(response);
                const data = JSON.parse(response.data);
                const fileData = data.Row[0];
                showPreviewStakModal(fileData);
            });
        }
    }

    renderContextMenu(fileData) {

        if (!this.props.data.options.enableContextMenu) {
            return null;
        }

        return (<div className="context-menu-container">
            <div className='context-menu-block' onClick={() => {this.clickPreview(fileData)}}>Preview |</div>
            <div className='context-menu-block add-to-stak' onClick={() => {this.props.showNewStakModal(fileData)}}> Add To Stak |</div>
            <div className='context-menu-block' onClick={() => {this.clickDownloadFile(fileData)}}> Download </div>
        </div>);
    }

    displayLayout() {

        const generateIconByFileExt = this.generateIconByFileExt;
        console.log('Actual Data');
        console.log(this.props.data);
        if (this.state.displayedLayout == this.displayedLayout.GRID) {
            return (
                <div className='div-file-content'>
                    {this.props.data && this.props.data.fileData && this.props.data.fileData.Row.map(fileData => {
                        const fileName = fileData.FILENAME || fileData._FILENAME || fileData.FOLDERNAME;

                        if (!fileName) {
                            return null;
                        }

                        return (
                            <div className="file-box" onClick={() => {this.props.onFileContentClick(fileData)}}>
                                <div className="file-img-box-container">
                                    <div className="file-img-box" onContextMenu={this.showContextMenu} data-id={fileData.FILEID}>
                                        {generateIconByFileExt(fileData.FILEEXT, fileData.FOLDERNAME)}
                                    </div>
                                </div>
                                <label className="wrap-text-content"> {fileName} </label>
                            </div>
                        );
                    })}
                </div>
            );
        } else {

            const generateIconByFileExt = this.generateIconByFileExt;

            const formatBytes = this.formatBytes;
            console.log('Actual Data');
            console.log(this.props.data);

            return (
                this.props.data && this.props.data.fileData && this.props.data.fileData.Row.map(fileData => {

                    const fileName = fileData.FILENAME || fileData._FILENAME || fileData.FOLDERNAME;

                    if (!fileName) {
                        return null;
                    }

                    return <div className="folder-detail-row" onClick={() => {this.props.onFileContentClick && this.props.onFileContentClick(fileData)}}>
                        <div className="home-folder-inline-position div-name home-folder-content">
                            <i className="file-icon">
                                {this.generateListIconByFileExt(fileData.FILEEXT, fileData.FOLDERNAME)}
                            </i>
                            <label>{fileName}</label>
                        </div>
                        <div className="home-folder-inline-position div-modified home-folder-content">
                            <label>{fileData.LASTMODIFIEDDATE}</label>
                        </div>
                        <div className="home-folder-inline-position div-date-added home-folder-content">
                            <label>{fileData.DATEADDED}</label>
                        </div>
                        <div className="home-folder-inline-position div-file-size home-folder-content">
                            <label>{this.formatBytes(fileData.FILESIZE, 2)}</label>
                        </div>
                        {this.renderContextMenu(fileData)}
                        <br/>
                        <hr/>
                    </div>
                })
            );
        }

    }

    generateIconByFileExt(fileExt, folderName) {

        if(folderName) {
            return (<img src={StakGreen}/>);
        }

        switch (fileExt) {
            case 'png':
                return (<img src={Png}/>);
            case '.jpg':
            case '.jpeg':
                return (<img src={Jpeg}/>);
            case '.pdf':
            case 'pdf':
                return (<img src={AdobeFile}/>);
            case '.txt':
            case 'txt':
                return (<img src={Txt}/>);
            case '.docx':
            case 'docx':
            case 'doc':
                return (<img src={WordFile}/>);
            case 'stak':
                return (<img src={StakGreen}/>);
            default:
                return (<img src={RandomFile}/>);
        }
    }

    generateListIconByFileExt(fileExt, folderName) {

        if(folderName) {
            return (<img className="img-center-content" src={StakGreenList}/>);
        }

        switch (fileExt) {
            case 'png':
                return (<img src={PngList}/>);
            case '.jpg':
            case '.jpeg':
                return (<img src={JpegList}/>);
            case '.pdf':
            case 'pdf':
                return (<img src={AdobeFileList}/>);
            case '.txt':
            case 'txt':
                return (<img src={TxtList}/>);
            case '.docx':
            case 'docx':
            case 'doc':
                return (<img src={WordFileList}/>);
            case 'stak':
                return (<img className="img-center-content" src={StakGreenList}/>);
            default:
                return (<img src={RandomFileList}/>);
        }
    }

    displayHeaderControls() {

        if (!this.props.data) {
            return (<div></div>);
        }

        let headers = this.state.displayedLayout == this.displayedLayout.GRID ? this.props.data.header.grid : this.props.data.header.list;

        let listIconClasses = 'arrow-down ';
        let gridIconClasses = 'arrow-down pull-right-0 ';
        let divClass = 'home-folder-inline-position div-icon';
        let iconClasses = this.props.data.header.iconClass;

        if (iconClasses && this.state.displayedLayout == this.displayedLayout.GRID) {
            listIconClasses += iconClasses.list;
            gridIconClasses += iconClasses.grid;
            divClass += ' col-6';
        }

        return (
            <div className='div-home-folder-headers'>
                {
                    headers.map(header => {
                        return (
                            <div className={header.className}>
                                <label>{header.label}</label>
                                <i className="arrow-down">
                                    <img src={DownArrow}/>
                                </i>
                            </div>
                        )
                    })
                }

                {/*<div className="home-folder-inline-position div-icon col-6">
                    <i className="arrow-down pull-right-88">
                        <img src={ListView} onClick={this.displayListLayout}/>
                    </i>
                    <i className="arrow-down pull-right-3">
                        <img src={GridView} onClick={this.displayGridLayout}/>
                    </i>
                </div>*/}
                <div className={divClass}>
                    <i className={listIconClasses}>
                        <img src={ListView} onClick={this.displayListLayout}/>
                    </i>
                    <i className={gridIconClasses}>
                        <img src={GridView} onClick={this.displayGridLayout}/>
                    </i>
                </div>
            </div>
        );

    }

    render() {

        return (
            <div>

                {this.displayHeaderControls()}

                <hr/>

                {this.displayLayout()}

            </div>
        );

    }

}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({showNewStakModal: showNewStakModal, showPreviewStakModal: showPreviewStakModal}, dispatch);
}

export default connect(null, mapDispatchToProps)(FileDisplayLayout);