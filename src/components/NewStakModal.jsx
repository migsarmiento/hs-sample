import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {
    hideNewStakModal
} from "../redux-store/actions/modal-actions";
import {loadTableData} from "../services/service";

class NewStakModal extends Component {

    constructor(props) {
        super(props);
        this.submitNewStackModalForm = this.submitNewStackModalForm.bind(this);
        this.onSelectStak = this.onSelectStak.bind(this);
    }

    componentWillMount() {
        this.setState({
            searchResults: [],
            selectedStak: {}
        });

        const tableData = loadTableData('DOCUMENTFOLDER');
        tableData.then((response) => {
            const searchResults = JSON.parse(response.data.replace(/\\\//g, "/"));
            this.setState({
                searchResults: searchResults.Row
            });
        }).catch((error) => {
            this.setState({
                searchResults: []
            });
        });
    }

    componentWillReceiveProps() {
        const tableData = loadTableData('DOCUMENTFOLDER');
        tableData.then((response) => {
            const searchResults = JSON.parse(response.data.replace(/\\\//g, "/"));
            this.setState({
                searchResults: searchResults.Row
            });
        }).catch((error) => {
            this.setState({
                searchResults: []
            });
        });
    }


    submitNewStackModalForm() {
        this.props.hideNewStakModal();

        if (this.state.selectedStak && this.props.tableItem) {
            console.log(this.props.tableItem);
            const params = {
                _USERID: "GLOBAL",
                _TABLENAME: this.state.selectedStak,
                _FILENAME: this.props.tableItem.FILENAME,
                _FILEEXT: this.props.tableItem.FILEEXT,
                _FILESIZE: this.props.tableItem.FILESIZE
            }

            const xmlhttp = new XMLHttpRequest();
            xmlhttp.open("POST", "http://10.10.1.202/api/enCapsaGlobalAPI/Submit/FormData", false);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            xmlhttp.onreadystatechange = function () {

                if (xmlhttp.readyState == XMLHttpRequest.DONE) {

                    if (xmlhttp.status == 200) {
                        alert("Successfully added to Stak");
                    } else {
                        alert("Something went wrong. Upload Failed.");
                    }

                }
            }

            xmlhttp.send(Object.keys(params).map(key => key + '=' + encodeURIComponent(params[key])).join('&'));
        }

    }

    onSelectStak(event) {
        const selectedStak = event.target.value;
        console.log('SELECTED STAK');
        console.log(selectedStak);
        this.setState({
            selectedStak: selectedStak
        });
    }

    render() {
        console.log('stak modal');
        console.log(this.state);
        if (this.props.displayModal) {
            return (
                <div className="div-modal-backdrop-container">
                    <div className="div-modal-container div-stak-modal">
                        <div className="div-modal-content">
                            <div className="div-modal-header">
                                <label>Add To Stak</label>
                                <button onClick={this.props.hideNewStakModal} className='div-stak-close'>Close X
                                </button>
                            </div>
                            <div className="div-modal-body div-stak-body">
                                <div className="div-new-stak-container div-stak-container">
                                    <div className="div-new-stak-row">
                                        <div className="div-row-left-side position-inline-class">
                                            <label>Staks</label>
                                        </div>
                                        <div className="div-row-right-side position-inline-class">
                                            <select className="div-stak-dropdown" onChange={this.onSelectStak}>
                                                <option selected="selected"> Select Stak</option>
                                                {this.state && this.state.searchResults.map((searchResult) => {
                                                    return <option
                                                        value={searchResult.APP_FORMID}>{searchResult.FOLDERNAME}</option>
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="div-modal-footer div-stak-footer">
                                <div className="div-modal-footer-button-group div-stak-footer-button-group">
                                    <button className="rounded-button-white-bg"
                                            onClick={this.props.hideNewStakModal}> Cancel
                                    </button>
                                    <button className="rounded-button-green-bg"
                                            onClick={this.submitNewStackModalForm}> Add
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }

    }

}

const mapStateToProps = (state) => {
    console.log('New Stak Modal State');
    console.log(state);
    return {
        tableItem: state.modal ? state.modal.tableItem : {}
    };
}

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        hideNewStakModal
    }, dispatch);
}

const NewStakModalContainer = connect(mapStateToProps, mapDispatchToProps)(NewStakModal);

export default NewStakModalContainer;